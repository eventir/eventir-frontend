import ISession from './ISession';
import IUser from './IUser';
/**
 * The root of the event type's schema.
 * @export
 * @interface IEvent
 */
export default interface IEvent {
    /**
     * The id of the event
     * @type {number}
     * @memberof IEvent
     */
    eventId: number;
    /**
     * The brief but declarative name of the event
     * @type {string}
     * @memberof IEvent
     */
    name: string;
    /**
     * A short description of the event
     * @type {string}
     * @memberof IEvent
     */
    description: string;
    /**
     * A URL to the image for the event
     * @type {string}
     * @memberof IEvent
     */
    image: string;
    /**
     * The sessions in the event
     * @type {Array<ISession>}
     * @memberof IEvent
     */
    sessions: Array<ISession>;
    /**
     * The admins that can edit the event
     * @type {Array<IUser>}
     * @memberof IEvent
     */
    eventAdmins: Array<IUser>;
    /**
     * The start DateTime of the event
     * @type {Date}
     * @memberof IEvent
     */
    start: Date;
    /**
     * The end DateTime of the event
     * @type {Date}
     * @memberof IEvent
     */
    end: Date;

    /**
     * The number of minutes before/after a session to allow checkins
     * @type {number}
     * @memberof IEvent
     */
    checkinWindow: number;
}
