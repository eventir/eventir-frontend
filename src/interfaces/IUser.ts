/**
 * The root of the user type's schema.
 * @export
 * @interface IUser
 */
export default interface IUser {
    /**
     * The user's first name
     * @type {string}
     * @memberof IUser
     */
    firstName: string;
    /**
     * The user's surname
     * @type {string}
     * @memberof IUser
     */
    lastName: string;
    /**
     * The user's email address
     * @type {string}
     * @memberof IUser
     */
    email: string;
    /**
     * The user's profile icon address
     * @type {string}
     * @memberof IUser
     */
    profileIcon: string;
    /**
     * The unique numeric identifier of the user
     * @type {number}
     * @memberof IUser
     */
    userId: string;
}
