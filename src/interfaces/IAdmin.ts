import IUser from './IUser';
/**
 * A system admin
 * @export
 * @interface IAdmin
 */
export default interface IAdmin {
    /**
     * The id of the admin
     * @type {number}
     * @memberof IAdmin
     */
    adminId: number;
    /**
     *
     * @type {IUser}
     * @memberof IAdmin
     */
    user: IUser;
}
