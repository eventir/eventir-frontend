import IUser from './IUser';
/**
 * The root of the team type's schema.
 * @export
 * @interface ITeam
 */
export default interface ITeam {
    /**
     * The unique numeric identifier of the team
     * @type {number}
     * @memberof Team
     */
    teamId: number;
    /**
     * The name that signifies the team
     * @type {string}
     * @memberof Team
     */
    name: string;
    /**
     * The ID of the user in charge of the team
     * @type {number}
     * @memberof Team
     */
    teamAdmin: IUser;
    /**
     * The members of the team
     * @type {Array<IUser>}
     * @memberof Team
     */
    members: Array<IUser>;
}
