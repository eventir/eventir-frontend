import ISession from './ISession';
import IUser from './IUser';
/**
 * A single checkin by a user for a session
 * @export
 * @interface ICheckin
 */
export default interface ICheckin {
    /**
     * The id of the checkin
     * @type {number}
     * @memberof ICheckin
     */
    checkinId: number;
    /**
     *
     * @type {number}
     * @memberof ICheckin
     */
    sessionId: number;
    /**
     *
     * @type {IUser}
     * @memberof ICheckin
     */
    user: IUser;
    /**
     * The time the user checked in to the event
     * @type {Date}
     * @memberof ICheckin
     */
    inTime: Date;
}
