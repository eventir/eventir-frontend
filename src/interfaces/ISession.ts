import ICheckin from './ICheckin';
import IRsvp from './IRsvp';
import IUser from './IUser';
/**
 * The root of the session type's schema.
 * @export
 * @interface ISession
 */
export default interface ISession {
    /**
     * The id of the session
     * @type {number}
     * @memberof ISession
     */
    sessionId: number;
    /**
     * The id of the event for this session
     * @type {number}
     * @memberof ISession
     */
    eventId: number;
    /**
     * The name of the session
     * @type {string}
     * @memberof ISession
     */
    name: string;
    /**
     * A short description of the session
     * @type {string}
     * @memberof ISession
     */
    description: string;
    /**
     * A URL to the image for the session
     * @type {string}
     * @memberof ISession
     */
    image: string;
    /**
     * A string description of the location of the session
     * @type {string}
     * @memberof ISession
     */
    location: string;
    /**
     * The start DateTime of the session
     * @type {Date}
     * @memberof ISession
     */
    start: Date;
    /**
     * The end DateTime of the session
     * @type {Date}
     * @memberof ISession
     */
    end: Date;
    /**
     * The users RSVP'd to the session
     * @type {Array<IRsvp>}
     * @memberof ISession
     */
    rsvps: Array<IRsvp>;
    /**
     * The users who have checked-in to the session.
     * @type (Array<ICheckin>)
     * @memberof ISession
     */
    checkins: Array<ICheckin>;
    /**
     * The admins that can edit the session
     * @type {Array<IUser>}
     * @memberof ISession
     */
    sessionAdmins: Array<IUser>;
}
