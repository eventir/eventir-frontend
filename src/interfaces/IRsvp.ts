import ISession from './ISession';
import IUser from './IUser';
/**
 * A single rsvp for a user to a session
 * @export
 * @interface IRsvp
 */
export default interface IRsvp {
    /**
     * The id of the RSVP
     * @type {number}
     * @memberof IRsvp
     */
    rsvpId: number;
    /**
     *
     * @type {IUser}
     * @memberof IRsvp
     */
    user: IUser;
    /**
     *
     * @type {number}
     * @memberof IRsvp
     */
    sessionId: number;
}
