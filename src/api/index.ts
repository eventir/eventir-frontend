interface CustomRequestInit extends RequestInit {
    body?: BodyInit | any;
}

export default class Api {
    static BaseUrl = 'http://localhost:5000/api';
    static DateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z?$/;

    static async call(url: string, args: CustomRequestInit = {}) {
        if (args.body != null && Api.shouldStringify(args.body)) {
            args.body = JSON.stringify(args.body);
        }
        args.headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type'
        };

        args.mode = 'cors';
        args.credentials = 'include';

        let response = await fetch(Api.BaseUrl + url, args);
        //Calling .json on an empty string errors, so catch it and return undefined
        try {
            let data = await response.text();
            return JSON.parse(data, Api.reviver);
        } catch (e) {}
    }

    static reviver = (key: string, value: any) => {
        if (typeof value === 'string' && Api.DateFormat.test(value)) {
            // Force parsing dates at GMT, as the server currently doesn't return a nice format
            // Plus dates are stupid, see https://stackoverflow.com/a/33909265 and https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse#Date_Time_String_Format
            if (value[value.length] !== 'Z') value += 'Z';

            return new Date(value);
        }

        return value;
    };

    static shouldStringify(body: any) {
        return !(
            body instanceof Blob ||
            body instanceof ArrayBuffer ||
            body instanceof FormData ||
            body instanceof URLSearchParams ||
            body instanceof ReadableStream ||
            typeof body === 'string'
        );
    }
}
