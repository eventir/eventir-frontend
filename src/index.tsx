import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './app';
import Login from './pages/login';
import Store from './redux/store';
import * as serviceWorker from './serviceWorker';

const root = document.getElementById('root');
const base = (app: typeof App) => (
    <Provider store={Store}>
        <BrowserRouter>
            <Switch>
                <Route exact path='/login' component={Login} />
                <Route path='/' component={app} />
            </Switch>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(base(App), root);

if (module.hot) {
    module.hot.accept('./app', () => {
        const NextApp = require('./app').default;
        ReactDOM.render(base(NextApp), root);
    });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
