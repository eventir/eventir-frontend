// Because we can't import this in SASS
import '@rmwc/icon/icon.css';
import '@rmwc/avatar/avatar.css';

import './index.scss';

import { Avatar } from '@rmwc/avatar';
import { Drawer, DrawerAppContent, DrawerContent, DrawerHeader, DrawerTitle } from '@rmwc/drawer';
import { Grid } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { List, SimpleListItem } from '@rmwc/list';
import { SimpleMenu } from '@rmwc/menu';
import { TopAppBar, TopAppBarFixedAdjust, TopAppBarRow, TopAppBarSection, TopAppBarTitle } from '@rmwc/top-app-bar';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Route, RouteComponentProps, Switch } from 'react-router-dom';
import { AnyAction, Dispatch } from 'redux';

import Api from './api';
import LoadingBar from './components/loading-bar';
import IUser from './interfaces/IUser';
import Event from './pages/event';
import EventList from './pages/event/list';
import Home from './pages/home';
import Session from './pages/session';
import Teams from './pages/teams';
import TeamEdit from './pages/teams/edit';
import UserProfileView from './pages/user';
import UserProfileEdit from './pages/user/edit';
import Users from './pages/users';
import { addCheckins } from './redux/actions/checkins';
import { setLoggedIn, setProfile } from './redux/actions/login';
import { addRSVPs } from './redux/actions/rsvps';
import { IStore } from './redux/store';

interface ILoginProps extends RouteComponentProps, IStore {
    setLoggedIn: (success: boolean) => void;
    setProfile: (user: IUser) => void;
    addRSVPs: (...sessions: number[]) => void;
    addCheckins: (...sessions: number[]) => void;
}

interface IAppState {
    navMenuOpen: boolean;
    userProfileImage: any;
}

class AppPresentation extends Component<ILoginProps, IAppState> {
    input: any;

    constructor(props: ILoginProps) {
        super(props);
        this.state = {
            navMenuOpen: true,
            userProfileImage: null
        };
    }

    async componentDidMount() {
        if (!this.props.loggedIn) {
            try {
                let profile = await Api.call('/users/current');
                if (profile == null) throw 'Not logged in';

                this.props.setLoggedIn(true);
                this.props.setProfile(profile);
                let postRoute = localStorage.getItem('postRoute');
                if (postRoute != null) {
                    this.props.history.push(postRoute);
                    localStorage.removeItem('postRoute');
                }
            } catch (e) {
                localStorage.setItem('postRoute', this.props.location.pathname);
                this.props.setLoggedIn(false);
                this.props.history.push('/login');
            }
        }

        await this.hydrate();
    }

    toggleNavMenu = () => {
        this.setState(prevState => ({ navMenuOpen: !prevState.navMenuOpen }));
    };

    setUserProfileImage = (el: HTMLImageElement) => {
        if (!this.state.userProfileImage) this.setState({ userProfileImage: el });
    };

    logout = () => {
        this.props.setLoggedIn(false);
        this.props.setProfile({ userId: '', firstName: '', lastName: '', profileIcon: '', email: '' });
        window.location.replace('http://localhost:5000/api/logout');
    };

    private async hydrate() {
        let [rsvps, checkins] = await Promise.all([
            Api.call(`/users/${this.props.profile.userId}/rsvps`),
            Api.call(`/users/${this.props.profile.userId}/checkins`)
        ]);
        if (rsvps) this.props.addRSVPs(...rsvps.map((r: { sessionId: number }) => r.sessionId));

        if (checkins) this.props.addCheckins(...checkins.map((c: { sessionId: number }) => c.sessionId));
    }

    render() {
        return (
            <React.Fragment>
                <TopAppBar fixed>
                    <TopAppBarRow>
                        <TopAppBarSection alignStart>
                            <Icon icon='menu' onClick={() => this.toggleNavMenu()} />
                            <TopAppBarTitle>Eventir</TopAppBarTitle>
                        </TopAppBarSection>
                        <TopAppBarSection alignEnd>
                            <SimpleMenu
                                handle={
                                    <Avatar
                                        src={this.props.profile.profileIcon}
                                        name={this.props.profile.firstName + ' ' + this.props.profile.lastName}
                                        size='xlarge'
                                        interactive
                                    />
                                }
                                anchorCorner='bottomStart'
                            >
                                <SimpleListItem role='menuitem' tabIndex={0} graphic='account_circle'>
                                    <Link to={`/users/${this.props.profile.userId}`}>Profile</Link>
                                </SimpleListItem>
                                <SimpleListItem
                                    role='menuitem'
                                    tabIndex={0}
                                    graphic='exit_to_app'
                                    text='Logout'
                                    onClick={this.logout}
                                />
                            </SimpleMenu>
                        </TopAppBarSection>
                    </TopAppBarRow>
                </TopAppBar>
                <TopAppBarFixedAdjust />
                <div className='drawer-container'>
                    <Drawer dismissible open={this.state.navMenuOpen}>
                        <DrawerHeader>
                            <DrawerTitle>Menu</DrawerTitle>
                        </DrawerHeader>

                        <DrawerContent>
                            <List>
                                <Link to='/'>
                                    <SimpleListItem graphic='home' text='Home' />
                                </Link>
                                <Link to='/events/'>
                                    <SimpleListItem graphic='event' text='Events' />
                                </Link>
                                <Link to='/teams'>
                                    <SimpleListItem graphic='people' text='Teams' />
                                </Link>
                                <Link to='/users'>
                                    <SimpleListItem graphic='person' text='Users' />
                                </Link>
                            </List>
                        </DrawerContent>
                    </Drawer>

                    <DrawerAppContent className='drawer-app-content'>
                        <LoadingBar />
                        <Grid>
                            <Switch>
                                <Route path='/events/:eventId/sessions/:sessionId' component={Session} />
                                <Route path='/events/:eventId' component={Event} />
                                <Route path='/events' component={EventList} />

                                <Route path='/teams/:id' component={TeamEdit} />
                                <Route path='/teams' component={Teams} />

                                <Route path='/userEdit' component={UserProfileEdit} />
                                <Route path='/users/:id' component={UserProfileView} />
                                <Route path='/users' component={Users} />

                                <Route path='/' component={Home} />
                            </Switch>
                        </Grid>
                    </DrawerAppContent>
                </div>
            </React.Fragment>
        );
    }
}

export default connect(
    (store: IStore) => ({
        ...store
    }),
    (dispatch: Dispatch<AnyAction>) => ({
        setLoggedIn: (success: boolean) => dispatch(setLoggedIn(success)),
        setProfile: (user: IUser) => dispatch(setProfile(user)),
        addRSVPs: (...sessionIds: number[]) => dispatch(addRSVPs(...sessionIds)),
        addCheckins: (...sessionIds: number[]) => dispatch(addCheckins(...sessionIds))
    })
)(AppPresentation);
