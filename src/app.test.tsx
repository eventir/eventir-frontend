import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import App from './app';
import Store from './redux/store';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={Store}>
            <BrowserRouter>
                <Route component={App} />
            </BrowserRouter>
        </Provider>,
        div
    );
    ReactDOM.unmountComponentAtNode(div);
});
