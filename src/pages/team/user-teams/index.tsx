import { Card } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';

import Api from '../../../api';
import UserTeamList from '../../../components/teams/user-team-list';
import ITeam from '../../../interfaces/ITeam';

interface IMyState {
    teams: Array<ITeam>;
    search: string;
}

export default class UserTeams extends Component<RouteComponentProps, IMyState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            teams: [],
            search: ''
        };
    }

    async componentDidMount() {
        await Api.call('/users/current').then(async user => {
            await Api.call('/users/' + user.userId + '/teams').then(teams => {
                try {
                    teams.forEach(async (team: { teamId: string }) => {
                        let res = await Api.call(`/teams/` + team.teamId);
                        if (res) {
                            this.state.teams.push(res);
                            this.setState(this.state);
                        }
                    });
                } catch {
                    this.setState(this.state);
                }
            });
        });
    }

    render() {
        return (
            <Card className='constrained-card'>
                <div className='header'>
                    <Typography use='headline4'>My Teams</Typography>
                </div>
                <div className='flex align-center padded'>
                    <TextField
                        className='grow'
                        label={'Search Teams'}
                        trailingIcon={<Icon role='button' icon='search' />}
                        outlined
                        value={this.state.search}
                        onChange={e => this.setState({ search: e.currentTarget.value })}
                    />
                </div>
                <div className='padded'>
                    <UserTeamList teams={this.state.teams} search={this.state.search} />
                </div>
            </Card>
        );
    }
}
