import { Card, CardPrimaryAction } from '@rmwc/card';
import { Grid, GridCell } from '@rmwc/grid';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Api from '../../api';
import SessionCard from '../../components/session-card';
import IEvent from '../../interfaces/IEvent';
import ISession from '../../interfaces/ISession';
import { IStore } from '../../redux/store';

interface IHomepageState {
    events: IEvent[];
    userRsvpSessions: ISession[];
}

class Homepage extends Component<IStore, IHomepageState> {
    fetchingRsvps = false;

    constructor(props: IStore) {
        super(props);

        this.state = {
            events: [],
            userRsvpSessions: []
        };
    }

    async componentDidMount() {
        await Promise.all([this.updateUserRsvps(), this.updateActiveEvents()]);
    }

    async componentDidUpdate(prevProps: IStore) {
        if (prevProps.profile.userId === this.props.profile.userId) return;

        await this.updateUserRsvps();
    }

    async updateUserRsvps() {
        if (!this.props.profile.userId) return;

        try {
            let userRsvpSessions = await Api.call(
                `/users/${this.props.profile.userId}/sessions?filterDate=${new Date().toISOString()}`
            );
            this.setState({ userRsvpSessions });
        } catch (e) {}
    }

    async updateActiveEvents() {
        let events = (await Api.call('/events')) as IEvent[];
        const now = new Date();
        events = events.filter(e => e.end > now);

        this.setState({ events });
    }

    render() {
        if (this.props.requests.left > 0) return;

        let activeEvents = this.state.events.map(e => e.name).join(', ');
        return (
            <React.Fragment>
                <GridCell span={12}>
                    <Card>
                        <div className='header'>
                            <Typography use='headline3'>Welcome, {this.props.profile.firstName}</Typography>
                        </div>
                        <CardPrimaryAction className='padded'>
                            <Typography use='headline5' className='no-margin'>
                                Active event
                                {this.state.events.length > 0 ? 's' : ''}
                            </Typography>
                            <Typography use='body1'>{activeEvents ? activeEvents : 'None'}</Typography>
                        </CardPrimaryAction>
                    </Card>
                </GridCell>
                <GridCell span={12}>
                    <Card>
                        <div className='header'>
                            <Typography use='headline4'>Your upcoming sessions</Typography>
                        </div>
                    </Card>
                </GridCell>
                {this.state.userRsvpSessions.map(s => (
                    <GridCell span={4} key={s.sessionId}>
                        <SessionCard session={s} />
                    </GridCell>
                ))}
            </React.Fragment>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(Homepage);
