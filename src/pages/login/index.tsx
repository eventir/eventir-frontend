import './login.scss';

import { Button } from '@rmwc/button';
import { Card } from '@rmwc/card';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { AnyAction, Dispatch } from 'redux';

import Api from '../../api';
import IUser from '../../interfaces/IUser';
import { setLoggedIn, setProfile, setToken } from '../../redux/actions/login';
import { IStore } from '../../redux/store';

interface ILoginProps extends RouteComponentProps, IStore {
    setLoggedIn: (success: boolean) => void;
    setProfile: (user: IUser) => void;
}

class LoginPresentation extends Component<ILoginProps> {
    constructor(props: ILoginProps) {
        super(props);
    }

    async componentDidMount() {
        if (!this.props.loggedIn) {
            let profile = await Api.call('/users/current');
            if (profile != null) {
                this.props.setLoggedIn(true);
                this.props.setProfile(profile);
                this.props.history.push('/');
            }
        } else this.props.history.push('/');
    }

    render() {
        return (
            <div className='login'>
                <Card className='mdc-card--login'>
                    <Typography use='overline'>Welcome to Eventir</Typography>
                    <br />
                    <a href='http://localhost:5000/api/login'>
                        <Button raised>Sign In</Button>
                    </a>
                </Card>
                <a
                    className='credit-badge'
                    href='https://unsplash.com/@eberhardgross?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge'
                    target='_blank'
                    rel='noopener noreferrer'
                    title='Download free do whatever you want high-resolution photos from eberhard grossgasteiger'
                >
                    <span>
                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'>
                            <title>unsplash-logo</title>
                            <path d='M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z' />
                        </svg>
                    </span>
                    <span>eberhard grossgasteiger</span>
                </a>
            </div>
        );
    }
}

export default connect(
    (store: IStore) => ({
        ...store
    }),
    (dispatch: Dispatch<AnyAction>) => ({
        setLoggedIn: (success: boolean) => dispatch(setLoggedIn(success)),
        setProfile: (user: IUser) => dispatch(setProfile(user))
    })
)(LoginPresentation);
