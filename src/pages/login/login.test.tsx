import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import Store from '../../redux/store';
import Login from '.';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={Store}>
            <BrowserRouter>
                <Route component={Login} />
            </BrowserRouter>
        </Provider>,
        div
    );
    ReactDOM.unmountComponentAtNode(div);
});
