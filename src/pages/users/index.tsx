import { Card } from '@rmwc/card';
import { GridCell } from '@rmwc/grid';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';

import Api from '../../api';
import UserList from '../../components/users/user-list';
import IUser from '../../interfaces/IUser';

interface IUserListState {
    users: Array<IUser>;
    search: string;
}

export default class Users extends Component<RouteComponentProps, IUserListState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            users: [],
            search: ''
        };
    }

    async componentDidMount() {
        await Api.call('/users').then(u => {
            this.setState({ users: u });
        });
    }

    render() {
        return (
            <GridCell span={8}>
                <Card className='teams'>
                    <div className='header'>
                        <Typography use='headline4'>Users</Typography>
                    </div>
                    <div className='flex align-center padded'>
                        <TextField
                            className='grow'
                            label={'Search Users'}
                            trailingIcon='search'
                            outlined
                            value={this.state.search}
                            onChange={e => this.setState({ search: e.currentTarget.value })}
                        />
                    </div>
                    <div className='padded'>
                        <UserList users={this.state.users} search={this.state.search} />
                    </div>
                </Card>
            </GridCell>
        );
    }
}
