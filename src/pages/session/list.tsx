import { Card } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Api from '../../api';
import SessionCard from '../../components/session-card';
import ISession from '../../interfaces/ISession';
import { IStore } from '../../redux/store';

interface ISessionListProps extends IStore {
    eventId: number;
}

interface ISessionListState {
    sessions: ISession[];
    search: string;
    viewPastEvents: boolean;
    isEventAdmin: boolean;
}

class SessionList extends Component<ISessionListProps, ISessionListState> {
    constructor(props: ISessionListProps) {
        super(props);
        this.state = {
            sessions: [],
            search: '',
            viewPastEvents: false,
            isEventAdmin: false
        };
    }

    async componentDidMount() {
        await Promise.all([this.updateIsAdmin(), this.updateSessions()]);
    }

    async componentDidUpdate(prevProps: ISessionListProps) {
        if (prevProps.profile.userId !== this.props.profile.userId) {
            await this.updateIsAdmin();
        }

        if (prevProps.eventId !== this.props.eventId) {
            await this.updateSessions();
        }
    }

    async updateIsAdmin() {
        if (!this.props.profile.userId) return;

        try {
            let isEventAdmin = await Api.call(`/events/${this.props.eventId}/admins/${this.props.profile.userId}`);
            this.setState({ isEventAdmin });
        } catch (e) {}
    }

    async updateSessions() {
        try {
            let sessions = await Api.call(`/events/${this.props.eventId}/sessions`);
            this.setState({ sessions });
        } catch (e) {}
    }

    render() {
        return (
            <React.Fragment>
                <Card className='margin-bottom'>
                    <div className='header'>
                        <Typography use='headline4'>Sessions</Typography>
                        {this.state.isEventAdmin && (
                            <Link to={this.props.eventId + '/sessions/new'} className='flex align-center'>
                                <Icon role='button' icon='add' style={{ marginLeft: 'auto' }} />
                                <Typography use='headline6'>Add Session</Typography>
                            </Link>
                        )}
                    </div>
                    <div className='padded flex'>
                        <TextField
                            className='grow'
                            label={'Search sessions'}
                            trailingIcon='search'
                            outlined
                            value={this.state.search}
                            onChange={e => this.setState({ search: e.currentTarget.value })}
                        />
                    </div>
                </Card>
                <GridCell span={12}>
                    <GridInner>
                        {this.state.sessions
                            .filter(s => s.name.toUpperCase().includes(this.state.search.toUpperCase()))
                            .map(session => (
                                <GridCell span={4} key={session.sessionId}>
                                    <SessionCard session={session} />
                                </GridCell>
                            ))}
                    </GridInner>
                </GridCell>
            </React.Fragment>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(SessionList);
