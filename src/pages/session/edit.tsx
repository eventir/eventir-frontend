import { Button } from '@rmwc/button';
import { Card, CardActionButtons, CardActions } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Switch } from '@rmwc/switch';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';

import Api from '../../api';
import DateTimeField from '../../components/DateTimeField/DateTimeField';
import ImageUpload from '../../components/image-upload';
import EmailNotification from '../../components/user-notify';
import ISession from '../../interfaces/ISession';

interface ISessionEditProps {
    session: ISession;
    isPreview: boolean;
    onSave: (session: ISession) => void;
    onCancel: () => void;
    onPreview: () => void;
}

interface ISessionEditState {
    session: ISession;
    errors: any;
}

export default class SessionEdit extends Component<ISessionEditProps, ISessionEditState> {
    constructor(props: ISessionEditProps) {
        super(props);
        this.state = {
            session: {
                ...props.session
            },
            errors: {}
        };
    }

    static getDerivedStateFromProps(props: ISessionEditProps, state: ISessionEditState) {
        let idsAreInts = Number.isInteger(props.session.sessionId) && Number.isInteger(state.session.sessionId);
        if (idsAreInts && props.session.sessionId !== state.session.sessionId) {
            return {
                ...state,
                session: {
                    ...props.session
                }
            };
        }

        return null;
    }

    makeDateWithTime(newDateString: string, base: Date) {
        let date = new Date(base);
        let newDate = new Date(newDateString);
        if (!(newDate instanceof Date) || isNaN(newDate.getTime())) return base;

        date.setFullYear(newDate.getFullYear());
        date.setMonth(newDate.getMonth());
        date.setDate(newDate.getDate());

        return date;
    }

    makeTimeWithDate(newDateString: string, base: Date) {
        let date = new Date(base);
        let newDate = new Date(newDateString);
        if (!(newDate instanceof Date) || isNaN(newDate.getTime())) return base;

        date.setHours(newDate.getHours());
        date.setMinutes(newDate.getMinutes());

        return date;
    }

    async save() {
        let method = 'POST';
        let url = `/events/${this.state.session.eventId}/sessions`;
        if (this.state.session.sessionId >= 0) {
            method = 'PUT';
            url = '/sessions/' + this.state.session.sessionId;
        }

        try {
            let res = await Api.call(url, { method, body: this.state.session });
            if (Number.isInteger(res.sessionId)) {
                this.props.onSave(res);
            } else {
                this.setState({ errors: { ...res } });
            }
        } catch (e) {
            // TODO: snackbar error
        }
    }

    render() {
        return (
            <Card>
                <GridInner className='padded'>
                    <GridCell span={6}>
                        <TextField
                            label='Name'
                            outlined
                            helpText={this.state.errors.name}
                            required
                            value={this.state.session.name}
                            onChange={e =>
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        name: e.currentTarget.value
                                    }
                                })
                            }
                        />
                        <br />
                        <br />
                        <TextField
                            label='Description'
                            textarea
                            outlined
                            helpText={this.state.errors.description}
                            value={this.state.session.description}
                            onChange={e =>
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        description: e.currentTarget.value
                                    }
                                })
                            }
                        />
                        <br />
                        <br />
                        <TextField
                            label='Location'
                            outlined
                            helpText={this.state.errors.location}
                            value={this.state.session.location}
                            onChange={e =>
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        location: e.currentTarget.value
                                    }
                                })
                            }
                        />
                    </GridCell>
                    <GridCell span={6}>
                        <ImageUpload
                            src={this.state.session.image}
                            onChange={(image: string) => {
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        image
                                    }
                                });
                            }}
                        />
                    </GridCell>
                    <GridCell span={12}>
                        <DateTimeField
                            labelPrefix='Start'
                            date={this.props.session.start}
                            onChange={start => {
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        start
                                    }
                                });
                            }}
                        />
                    </GridCell>
                    <GridCell span={12}>
                        <DateTimeField
                            labelPrefix='End'
                            date={this.props.session.end}
                            onChange={end =>
                                this.setState({
                                    session: {
                                        ...this.state.session,
                                        end
                                    }
                                })
                            }
                        />
                    </GridCell>
                    {/* TODO: session admins */}
                    <GridCell span={4} className='flex align-center justify-space-between'>
                        <Typography use='body1'>Preview</Typography>
                        <Switch checked={this.props.isPreview} onChange={() => this.props.onPreview()} />
                    </GridCell>
                </GridInner>
                <CardActions>
                    <CardActionButtons>
                        <Button onClick={() => this.save()}>Save</Button>
                        <Button onClick={() => this.props.onCancel()}>Cancel</Button>
                        <EmailNotification id={this.state.session.sessionId} type='session' />
                    </CardActionButtons>
                </CardActions>
            </Card>
        );
    }
}
