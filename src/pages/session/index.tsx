import './session.scss';

import { GridCell } from '@rmwc/grid';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';

import Api from '../../api';
import ISession from '../../interfaces/ISession';
import { IStore } from '../../redux/store';
import SessionEdit from './edit';
import SessionView from './view';

interface ISessionState {
    session: ISession;
    isSessionAdmin: boolean;
    isPreview: boolean;
}

interface ISessionProps extends IStore, RouteComponentProps<{ eventId: string; sessionId: string }> {}

class Session extends Component<ISessionProps, ISessionState> {
    constructor(props: ISessionProps) {
        super(props);

        this.state = {
            session: {
                eventId: Number.parseInt(this.props.match.params.eventId),
                sessionId: -1,
                name: '',
                description: '',
                image: '',
                location: '',
                start: new Date(),
                end: new Date(),
                rsvps: [],
                checkins: [],
                sessionAdmins: []
            },
            isSessionAdmin: false,
            isPreview: false
        };
    }

    async componentDidMount() {
        await Promise.all([this.updateIsAdmin(), this.updateSession()]);
    }

    async componentDidUpdate(prevProps: ISessionProps) {
        if (prevProps.profile.userId !== this.props.profile.userId) {
            await this.updateIsAdmin();
        }
        if (prevProps.match.params.sessionId !== this.props.match.params.sessionId) {
            await this.updateSession();
        }
    }

    async updateIsAdmin() {
        if (!this.props.profile.userId) return;

        try {
            let sessionId = Number.parseInt(this.props.match.params.sessionId);
            if (!Number.isInteger(sessionId)) {
                // This really shouldn't happen
                let eventId = Number.parseInt(this.props.match.params.eventId);
                if (!Number.isInteger(eventId)) {
                    this.props.history.goBack();
                }
                let isEventAdmin = await Api.call(`/events/${eventId}/admins/${this.props.profile.userId}`);
                this.setState({ isSessionAdmin: isEventAdmin });
            } else {
                let isSessionAdmin = await Api.call(`/sessions/${sessionId}/admins/${this.props.profile.userId}`);
                this.setState({ isSessionAdmin });
            }
        } catch (e) {}
    }

    async updateSession() {
        let sessionId = Number.parseInt(this.props.match.params.sessionId);
        if (!Number.isInteger(sessionId)) return;

        try {
            let session = await Api.call(`/sessions/${sessionId}`);
            this.setState({ session });
        } catch (e) {}
    }

    save(session: ISession) {
        this.setState({ session });
        this.props.history.goBack();
    }

    render() {
        return (
            <React.Fragment>
                {this.state.isSessionAdmin && (
                    <GridCell span={4} tablet={8} desktop={5}>
                        <SessionEdit
                            session={this.state.session}
                            onSave={session => {
                                this.save(session);
                            }}
                            onCancel={() => {
                                this.props.history.goBack();
                            }}
                            isPreview={this.state.isPreview}
                            onPreview={() => {
                                this.setState({ isPreview: !this.state.isPreview });
                            }}
                        />
                    </GridCell>
                )}
                {(!this.state.isSessionAdmin || this.state.isPreview) && (
                    <GridCell span={4} tablet={8} desktop={7}>
                        <SessionView session={this.state.session} isSessionAdmin={this.state.isSessionAdmin} />
                    </GridCell>
                )}
            </React.Fragment>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(Session);
