import { Avatar } from '@rmwc/avatar';
import { Card, CardActions, CardMedia, CardPrimaryAction } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
// @ts-ignore
import {RadialChart} from 'react-vis';

import RsvpCheckin from '../../components/rsvp-checkin';
import ISession from '../../interfaces/ISession';
import IUser from '../../interfaces/IUser';

interface ISessionViewProps {
    session: ISession;
    isSessionAdmin: boolean;
}

interface ISessionViewState {
    attendeeFilter: string;
}

export default class SessionView extends Component<ISessionViewProps, ISessionViewState> {
    constructor(props: ISessionViewProps) {
        super(props);

        this.state = {
            attendeeFilter: ''
        };
    }

    userNameContains(user: IUser, search: string) {
        search = search.trim();
        if (search.length === 0) return true;

        for (let term of search.split(' ')) {
            if (user.firstName.includes(term) || user.lastName.includes(term)) return true;
        }
        return false;
    }

    rsvpsNotCheckedIn() {
        return this.props.session.rsvps.filter(
            r => !this.props.session.checkins.map(c => c.user.userId).includes(r.user.userId)
        );
    }

    render() {
        let groups = this.props.session.checkins.reduce(
            (accumulator: any, checkin) => {
                let minute = checkin.inTime.getMinutes();
                accumulator[minute] = accumulator[minute] + 1 || 1;
                return accumulator;
            },
            {} as any
        );

        let barChartData = [
            ...Object.keys(groups).map(key => {
                return {
                    x: key,
                    y: groups[key]
                };
            })
        ];

        console.log(barChartData);

        return (
            <Card className='session-view'>
                {this.props.session.image && (
                    <CardMedia sixteenByNine style={{ backgroundImage: `url(${this.props.session.image})` }} />
                )}
                <div className='padded'>
                    <Typography use='headline4'>{this.props.session.name}</Typography>
                    <br />
                    <Typography use='headline5'>{this.props.session.description}</Typography>
                    <br />
                    <br />
                    <GridInner>
                        <GridCell span={6}>
                            <Typography use='headline6'>Location</Typography>
                            <br />
                            <Typography use='body2'>{this.props.session.location}</Typography>
                        </GridCell>
                        <GridCell span={6}>
                            <Typography use='headline6'>Time</Typography>
                            <br />
                            <Typography use='body2'>{this.props.session.start.toLocaleDateString()}</Typography>
                            <Typography use='body2'>
                                {this.props.session.start.toLocaleTimeString()}–
                                {this.props.session.end.toLocaleTimeString()}
                            </Typography>
                        </GridCell>
                    </GridInner>
                    <br />
                    <div className='flex align-center'>
                        <Typography use='headline5' className='margin-right'>
                            Who's coming
                        </Typography>
                        <TextField
                            className='grow'
                            label='Search'
                            icon='search'
                            outlined
                            value={this.state.attendeeFilter}
                            onChange={e =>
                                this.setState({
                                    attendeeFilter: e.currentTarget.value
                                })
                            }
                        />
                    </div>
                    <br />
                    <GridInner className='attendees'>
                        {this.props.session.checkins
                            .map(c => c.user)
                            .filter(u => this.userNameContains(u, this.state.attendeeFilter))
                            .map(u => (
                                <GridCell span={6} className='flex align-center' key={u.userId}>
                                    <Avatar src={u.profileIcon} className='margin-right' />
                                    <Typography use='body2'>
                                        {u.firstName} {u.lastName}
                                    </Typography>
                                    <Icon icon='check_circle' style={{ marginLeft: 'auto' }} />
                                </GridCell>
                            ))}
                        {this.rsvpsNotCheckedIn()
                            .map(r => r.user)
                            .filter(u => this.userNameContains(u, this.state.attendeeFilter))
                            .map(u => (
                                <GridCell span={6} className='attendee flex align-center' key={u.userId}>
                                    <Avatar src={u.profileIcon} className='margin-right' />
                                    <Typography use='body2'>
                                        {u.firstName} {u.lastName}
                                    </Typography>
                                </GridCell>
                            ))}
                    </GridInner>
                    <br />
                    <br />
                    <GridInner>
                        {this.props.session.rsvps.length > 0 && (
                            <GridCell span={4}>
                                <RadialChart
                                    width={250}
                                    height={250}
                                    data={[
                                        {
                                            angle: this.props.session.checkins.length,
                                            label: 'Checkins'
                                        },
                                        {
                                            angle: this.rsvpsNotCheckedIn().length,
                                            label: 'RSVPs'
                                        }
                                    ]}
                                    innerRadius={60}
                                    radius={100}
                                    showLabels
                                    labelsRadiusMultiplier={1.25}
                                    padAngle={0.08}
                                />
                            </GridCell>
                        )}
                    </GridInner>
                </div>
                <CardActions>
                    <RsvpCheckin session={this.props.session} />
                </CardActions>
            </Card>
        );
    }
}
