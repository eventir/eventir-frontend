import './user.scss';

import { Avatar } from '@rmwc/avatar';
import { Button } from '@rmwc/button';
import { Card } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

import Api from '../../api';
import SmallEventList from '../../components/small-event-list';
import SmallSessionList from '../../components/small-session-list';
import IEvent from '../../interfaces/IEvent';
import ISession from '../../interfaces/ISession';
import IUser from '../../interfaces/IUser';

interface IUserProfileState {
    user: IUser;
    currentUser: IUser;
    sessions: ISession[];
    events: IEvent[];
    sessionSearch: string;
    eventSearch: string;
    isAdmin: boolean;
}

export default class UserProfile extends Component<RouteComponentProps<{ id: string }>, IUserProfileState> {
    constructor(props: any) {
        super(props);
        this.state = {
            user: {
                userId: '',
                firstName: '',
                lastName: '',
                profileIcon: '',
                email: ''
            },
            currentUser: {
                userId: '',
                firstName: '',
                lastName: '',
                profileIcon: '',
                email: ''
            },
            sessions: [],
            events: [],
            sessionSearch: '',
            eventSearch: '',
            isAdmin: false
        };
    }

    async componentDidMount() {
        this.getMyData();
    }

    async componentDidUpdate(prevProps: any, prevState: any) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            //clear everything out
            this.state = {
                user: {
                    userId: '',
                    firstName: '',
                    lastName: '',
                    profileIcon: '',
                    email: ''
                },
                currentUser: {
                    userId: '',
                    firstName: '',
                    lastName: '',
                    profileIcon: '',
                    email: ''
                },
                sessions: [],
                events: [],
                sessionSearch: '',
                eventSearch: '',
                isAdmin: false
            };

            //now get the actual values
            this.getMyData();
        }
    }

    async getMyData() {
        //if the user is looking at themselves
        if (this.props.match.params.id === 'current') {
            await Api.call('/users/current').then(async user => {
                this.setState({ user: user });
                this.setState({ currentUser: user });
                await Api.call('/users/' + this.state.user.userId + '/sessions').then(sessions => {
                    this.setState({ sessions: sessions });
                });
                await Api.call('/users/' + this.state.user.userId + '/events').then(events => {
                    this.setState({ events: events });
                });
                await Api.call('/admins/' + this.state.user.userId).then(isAdmin => {
                    this.setState({ isAdmin: isAdmin });
                });
            });
        } else {
            await Api.call('/users/' + this.props.match.params.id).then(user => {
                this.setState({ user: user });
            });
            await Api.call('/users/' + this.props.match.params.id + '/sessions').then(sessions => {
                this.setState({ sessions: sessions });
            });
            await Api.call('/users/' + this.props.match.params.id + '/events').then(events => {
                this.setState({ events: events });
            });
            await Api.call('/users/current').then(async user => {
                this.setState({ currentUser: user });
                await Api.call('/admins/' + this.state.currentUser.userId).then(isAdmin => {
                    this.setState({ isAdmin: isAdmin });
                });
            });
        }
    }

    render() {
        const style = {
            backgroundColor: 'white',
            borderRadius: '.3em'
        };

        //should only be able to edit if admin or the user to be edited
        let edit = null;
        if (this.state.isAdmin || this.state.currentUser.userId === this.state.user.userId) {
            edit = (
                <Link
                    className='save padded-half'
                    to={{
                        pathname: '/userEdit',
                        state: {
                            user: this.state.user,
                            currentUser: this.state.currentUser,
                            userId: this.state.user.userId,
                            currentUserId: this.state.currentUser.userId
                        }
                    }}
                >
                    <Button label='Edit' icon='edit' />
                </Link>
            );
        }
        return (
            <GridCell span={8}>
                <Card className='margin-bottom'>
                    <span className='header'>
                        <Typography use='headline3' className='title'>
                            {this.state.user.firstName + ' ' + this.state.user.lastName}
                        </Typography>
                        <Avatar
                            style={{
                                width: '4rem',
                                height: '4rem'
                            }}
                            src={this.state.user.profileIcon}
                        />
                    </span>
                    {edit}
                </Card>
                <GridInner>
                    <GridCell span={6}>
                        <Card className='padded'>
                            <TextField
                                style={style}
                                label={"RSVP'd Sessions"}
                                trailingIcon={<Icon icon='search' />}
                                outlined
                                className='textField'
                                value={this.state.sessionSearch}
                                onChange={e =>
                                    this.setState({
                                        sessionSearch: e.currentTarget.value
                                    })
                                }
                            />
                            <div className='scrollArea'>
                                <SmallSessionList sessions={this.state.sessions} search={this.state.sessionSearch} />
                            </div>
                        </Card>
                    </GridCell>
                    <GridCell span={6}>
                        <Card className='padded'>
                            <TextField
                                style={style}
                                className='margin-bottom'
                                label={'Attended Events'}
                                trailingIcon={<Icon icon='search' />}
                                outlined
                                value={this.state.eventSearch}
                                onChange={e =>
                                    this.setState({
                                        eventSearch: e.currentTarget.value
                                    })
                                }
                            />
                            <SmallEventList events={this.state.events} search={this.state.eventSearch} />
                        </Card>
                    </GridCell>
                </GridInner>
            </GridCell>
        );
    }
}
