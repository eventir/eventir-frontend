import './user.scss';

import { Avatar } from '@rmwc/avatar';
import { Button } from '@rmwc/button';
import { Card } from '@rmwc/card';
import { Dialog, DialogActions, DialogButton } from '@rmwc/dialog';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';

import Api from '../../../api';
import ImageUpload from '../../../components/image-upload';
import IUser from '../../../interfaces/IUser';
import { setProfile } from '../../../redux/actions/login';
import Store, { IStore } from '../../../redux/store';

interface IUserProfileState {
    currentUser: IUser;
    constUser: IUser;
    tempUser: IUser;
    isAdminCurrent: boolean;
    isAdminUserOrig: boolean;
    isAdminUser: boolean;
    uploadImage: boolean;
    errors: boolean[];
}

const errorFields = {
    FIRSTNAME: 0,
    LASTNAME: 1,
    EMAIL: 2,
    DESCRIPTION: 3
};

export default class UserProfileEdit extends Component<RouteComponentProps, IUserProfileState> {
    constructor(props: any) {
        super(props);
        this.state = {
            currentUser: {
                userId: '',
                firstName: '',
                lastName: '',
                profileIcon: '',
                email: ''
            },
            constUser: {
                userId: '',
                firstName: '',
                lastName: '',
                profileIcon: '',
                email: ''
            },
            tempUser: {
                userId: '',
                firstName: '',
                lastName: '',
                profileIcon: '',
                email: ''
            },
            isAdminCurrent: false,
            isAdminUser: false,
            isAdminUserOrig: false,
            uploadImage: false,
            errors: [false, false, false, false]
        };
    }

    async componentDidMount() {
        this.setState({
            currentUser: {
                ...this.props.location.state.currentUser
            }
        });
        this.setState({
            constUser: {
                ...this.props.location.state.user
            }
        });
        this.setState({
            tempUser: {
                ...this.props.location.state.user
            }
        });
        await Api.call('/admins/' + this.props.location.state.userId).then(isAdmin => {
            this.setState({ isAdminUser: isAdmin });
            this.setState({ isAdminUserOrig: isAdmin });
        });
        await Api.call('/admins/' + this.props.location.state.currentUserId).then(isAdmin => {
            this.setState({ isAdminCurrent: isAdmin });
        });
    }

    //handles making sure the email entered is in a valid email form
    updateEmail(e: FormEvent<HTMLTextAreaElement>) {
        let email = e.currentTarget.value;
        let emailPattern = new RegExp(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        if (emailPattern.test(email)) {
            let temp = this.state.errors;
            temp[errorFields.EMAIL] = false;
            this.setState({
                tempUser: {
                    ...this.state.tempUser,
                    email: email
                },
                errors: temp
            });
        } else {
            let temp = this.state.errors;
            temp[errorFields.EMAIL] = true;
            this.setState({
                tempUser: {
                    ...this.state.tempUser,
                    email: email
                },
                errors: temp
            });
        }
    }

    handleSave = async () => {
        let method = 'PUT';
        let url = '/users/' + this.state.constUser.userId;
        let res;
        //make sure there aren't any problems with the fields
        if (!this.state.errors.includes(true)) {
            await Api.call(url, { method, body: this.state.tempUser }).then(async () => {
                //see if the status of admin has even changed
                if (this.state.isAdminUser !== this.state.isAdminUserOrig) {
                    if (this.state.isAdminUser) {
                        await Api.call('/admins/' + this.props.location.state.userId, { method: 'POST' }).then(() => {
                            res = true;
                        });
                    } else {
                        await Api.call('/admins/' + this.props.location.state.userId, { method: 'DELETE' }).then(() => {
                            res = true;
                        });
                    }
                } else {
                    res = true;
                }
            });

            if (res === true) {
                Store.dispatch(setProfile(this.state.tempUser));
                this.props.history.goBack();
            }
        } else {
            //TO DO: add toaster
        }
    };

    handleCancel = () => {
        this.props.history.goBack();
    };

    imageUploadToggler = async () => {
        //toggles image upload space back and forth
        if (this.state.uploadImage) {
            this.setState({
                uploadImage: false
            });
        } else {
            this.setState({
                uploadImage: true
            });
        }
    };

    imageUploadHandler = (url: string) => {
        let newUser: IUser;
        newUser = { ...this.state.currentUser };
        newUser.profileIcon = url;
        this.setState({ tempUser: { ...newUser } });
        this.imageUploadToggler();
    };

    promoteToAdminHandler = () => {
        if (this.state.isAdminUser) {
            this.setState({ isAdminUser: false });
        } else {
            this.setState({ isAdminUser: true });
        }
    };

    cancelImageUploadToggler = () => {
        this.setState({ uploadImage: false });
    };

    render() {
        //handles styling for errors in validation
        let emailStyle = {};
        if (this.state.errors[errorFields.EMAIL]) {
            emailStyle = {
                outline: 'auto',
                outlineWidth: 'thick',
                outlineColor: 'red'
            };
        }

        //handles if it should show the admin check boxes or not.
        //show them if the current user is an admin.
        //I used the material icon check box here instead of a web component checkbox, because
        //it kept giving me errors that I couldn't work out.
        let promoteToAdmin = null;
        if (this.state.isAdminCurrent) {
            if (this.state.isAdminUser) {
                promoteToAdmin = <Button label='Admin' icon='check_box' onClick={this.promoteToAdminHandler} />;
            } else {
                promoteToAdmin = (
                    <Button label='Admin' icon='check_box_outline_blank' onClick={this.promoteToAdminHandler} />
                );
            }
        }

        //handles showing the image upload space or not
        //handles showing button for editting user profile pic
        let imageEdit = null;
        if (this.state.constUser.userId === this.state.currentUser.userId) {
            imageEdit = <Icon icon='edit' onClick={this.imageUploadToggler} style={{ marginLeft: '1rem' }} />;
        }

        return (
            <GridCell span={8} className=''>
                <Card className='margin-bottom'>
                    <span className='header justify-space-between'>
                        <Typography use='headline3' className='title'>
                            {this.state.constUser.firstName + ' ' + this.state.constUser.lastName}
                        </Typography>
                        <span className='flex align-center'>
                            <Avatar
                                style={{
                                    width: '4rem',
                                    height: '4rem'
                                }}
                                src={this.state.tempUser.profileIcon}
                            />
                            {imageEdit}
                        </span>
                    </span>
                    <span className='padded-half flex'>
                        {promoteToAdmin}
                        <Button label='Cancel' icon='cancel' onClick={this.handleCancel} />
                        <Button label='Save' icon='check' onClick={this.handleSave} />
                    </span>
                </Card>
                <Dialog open={this.state.uploadImage} className='imageUploadArea'>
                    <ImageUpload
                        src={this.state.constUser.profileIcon as string}
                        square={Boolean(true)}
                        minWidth={256}
                        minHeight={256}
                        onChange={this.imageUploadHandler}
                    />
                    <DialogActions>
                        <DialogButton onClick={this.imageUploadToggler}>Done</DialogButton>
                    </DialogActions>
                </Dialog>
                <GridInner>
                    <GridCell span={6}>
                        <Card className='padded'>
                            <TextField
                                label='First Name'
                                icon='person'
                                outlined
                                value={this.state.tempUser.firstName}
                                onChange={e =>
                                    this.setState({
                                        tempUser: {
                                            ...this.state.tempUser,
                                            firstName: e.currentTarget.value
                                        }
                                    })
                                }
                            />
                            <br />
                            <TextField
                                label='Last Name'
                                outlined
                                value={this.state.tempUser.lastName}
                                onChange={e =>
                                    this.setState({
                                        tempUser: {
                                            ...this.state.tempUser,
                                            lastName: e.currentTarget.value
                                        }
                                    })
                                }
                            />
                        </Card>
                    </GridCell>
                    <GridCell span={6}>
                        <Card className='padded'>
                            <TextField
                                label='Email'
                                icon='mail'
                                outlined
                                style={emailStyle}
                                value={this.state.tempUser.email}
                                onChange={e => this.updateEmail(e)}
                            />
                        </Card>
                    </GridCell>
                </GridInner>
            </GridCell>
        );
    }
}
