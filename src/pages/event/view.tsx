import { Card, CardMedia, CardPrimaryAction } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React from 'react';
import { Link } from 'react-router-dom';

import IEvent from '../../interfaces/IEvent';

interface IEventViewProps {
    event: IEvent;
}

export default class EventView extends React.Component<IEventViewProps, {}> {
    render() {
        return (
            <Card>
                <Link to={'/events/' + this.props.event.eventId}>
                    <CardPrimaryAction>
                        {this.props.event.image && (
                            <CardMedia sixteenByNine style={{ backgroundImage: `url(${this.props.event.image})` }} />
                        )}
                        <div className='padded'>
                            <Typography use='headline4'>{this.props.event.name}</Typography>
                            <br />
                            <Typography use='headline5'>{this.props.event.description}</Typography>
                            <br />
                            <br />
                            <Typography use='body1' className='flex align-center no-margin'>
                                <Icon icon='event' className='margin-right' />
                                {this.props.event.start.toLocaleDateString()}–
                                {this.props.event.end.toLocaleDateString()}
                            </Typography>
                        </div>
                    </CardPrimaryAction>
                </Link>
            </Card>
        );
    }
}
