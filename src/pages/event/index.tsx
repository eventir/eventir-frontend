import { GridCell } from '@rmwc/grid';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';

import Api from '../../api';
import IEvent from '../../interfaces/IEvent';
import { IStore } from '../../redux/store';
import SessionList from '../session/list';
import EventEdit from './edit';
import EventView from './view';

interface IEventState {
    event: IEvent;
    isEventAdmin: boolean;
    isPreview: boolean;
}

interface IEventProps extends IStore, RouteComponentProps<{ eventId: string }> {}

class Event extends Component<IEventProps, IEventState> {
    constructor(props: IEventProps) {
        super(props);

        this.state = {
            event: {
                eventId: Number.parseInt(this.props.match.params.eventId),
                name: '',
                description: '',
                image: '',
                start: new Date(),
                end: new Date(),
                sessions: [],
                eventAdmins: [],
                checkinWindow: 0
            },
            isEventAdmin: false,
            isPreview: false
        };
    }

    async componentDidMount() {
        await Promise.all([this.updateEvent(), this.updateIsAdmin()]);
    }

    async componentDidUpdate(prevProps: IEventProps) {
        if (prevProps.match.params.eventId !== this.props.match.params.eventId) {
            await this.updateEvent();
        }
        if (prevProps.profile.userId != this.props.profile.userId) {
            await this.updateIsAdmin();
        }
    }

    async updateIsAdmin() {
        if (!this.props.profile.userId) return;

        try {
            let eventId = Number.parseInt(this.props.match.params.eventId);
            if (Number.isInteger(eventId)) {
                let isEventAdmin = await Api.call(`/events/${eventId}/admins/${this.props.profile.userId}`);
                this.setState({ isEventAdmin });
            } else {
                let isSuperAdmin = await Api.call(`/admins/${this.props.profile.userId}`);
                this.setState({ isEventAdmin: isSuperAdmin });
            }
        } catch (e) {}
    }

    async updateEvent() {
        let eventId = Number.parseInt(this.props.match.params.eventId);
        if (!Number.isInteger(eventId)) return;

        try {
            let event = await Api.call(`/events/${eventId}`);
            this.setState({ event });
            await this.updateIsAdmin();
        } catch (e) {}
    }

    save(event: IEvent) {
        this.setState({ event });
        this.props.history.goBack();
    }

    render() {
        return (
            <React.Fragment>
                {this.state.isEventAdmin && (
                    <GridCell span={4} tablet={8} desktop={5}>
                        <EventEdit
                            event={this.state.event}
                            onSave={event => {
                                this.save(event);
                            }}
                            onCancel={() => {
                                this.props.history.goBack();
                            }}
                            isPreview={this.state.isPreview}
                            onPreview={() => {
                                this.setState({ isPreview: !this.state.isPreview });
                            }}
                        />
                    </GridCell>
                )}
                {(!this.state.isEventAdmin || this.state.isPreview) && (
                    <GridCell span={4} tablet={8} desktop={7}>
                        <EventView event={this.state.event} />
                    </GridCell>
                )}
                <GridCell span={12}>
                    <SessionList eventId={this.state.event.eventId} />
                </GridCell>
            </React.Fragment>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(Event);
