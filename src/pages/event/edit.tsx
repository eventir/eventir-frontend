import { Button } from '@rmwc/button';
import { Card, CardActionButtons, CardActions } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Switch } from '@rmwc/switch';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React from 'react';

import Api from '../../api';
import DateTimeField from '../../components/DateTimeField/DateTimeField';
import ImageUpload from '../../components/image-upload';
import EmailNotification from '../../components/user-notify';
import IEvent from '../../interfaces/IEvent';

interface IEventEditProps {
    event: IEvent;
    isPreview: boolean;
    onSave: (event: IEvent) => void;
    onCancel: () => void;
    onPreview: () => void;
}

interface IEventEditState {
    event: IEvent;
    errors: any;
}

export default class EventEdit extends React.Component<IEventEditProps, IEventEditState> {
    constructor(props: any) {
        super(props);
        this.state = {
            event: {
                eventId: -1,
                name: '',
                description: '',
                image: '',
                sessions: [],
                eventAdmins: [],
                start: new Date(),
                end: new Date(),
                checkinWindow: 0
            },
            errors: {}
        };
    }

    static getDerivedStateFromProps(props: IEventEditProps, state: IEventEditState) {
        let idsAreInts = Number.isInteger(props.event.eventId) && Number.isInteger(state.event.eventId);
        if (idsAreInts && props.event.eventId !== state.event.eventId) {
            return {
                ...state,
                event: {
                    ...props.event
                }
            };
        }

        return null;
    }

    async save() {
        let method = 'POST';
        let url = '/events';
        if (this.state.event.eventId >= 0) {
            method = 'PUT';
            url += '/' + this.state.event.eventId;
        }

        try {
            let res = await Api.call(url, { method, body: this.state.event });
            if (Number.isInteger(res.eventId)) {
                this.props.onSave(res);
            } else {
                this.setState({ errors: { ...res } });
            }
        } catch (e) {}
    }

    render() {
        return (
            <Card>
                <GridInner className='padded'>
                    <GridCell span={6}>
                        <TextField
                            label='Name'
                            outlined
                            helpText={this.state.errors.name}
                            required
                            value={this.state.event.name}
                            onChange={e =>
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        name: e.currentTarget.value
                                    }
                                })
                            }
                        />
                        <br />
                        <br />
                        <TextField
                            label='Description'
                            textarea
                            outlined
                            helpText={this.state.errors.description}
                            required
                            value={this.state.event.description}
                            onChange={e =>
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        description: e.currentTarget.value
                                    }
                                })
                            }
                        />
                        <br />
                        <br />
                        <TextField
                            label='Checkin window'
                            outlined
                            helpText={
                                this.state.errors.checkinWindow ||
                                'The number of minutes before and after a session start to allow checkins'
                            }
                            required
                            value={this.state.event.checkinWindow}
                            onChange={e =>
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        checkinWindow: e.currentTarget.value
                                    }
                                })
                            }
                        />
                    </GridCell>
                    <GridCell span={6}>
                        <ImageUpload
                            src={this.state.event.image}
                            onChange={(image: string) => {
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        image
                                    }
                                });
                            }}
                        />
                    </GridCell>
                    <GridCell span={12}>
                        <DateTimeField
                            labelPrefix='Start'
                            date={this.props.event.start}
                            onChange={start => {
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        start
                                    }
                                });
                            }}
                        />
                    </GridCell>
                    <GridCell span={12}>
                        <DateTimeField
                            labelPrefix='End'
                            date={this.props.event.end}
                            onChange={end =>
                                this.setState({
                                    event: {
                                        ...this.state.event,
                                        end
                                    }
                                })
                            }
                        />
                    </GridCell>
                    {/* TODO: event admins */}
                    <GridCell span={4} className='flex align-center justify-space-between'>
                        <Typography use='body1'>Preview</Typography>
                        <Switch checked={this.props.isPreview} onChange={() => this.props.onPreview()} />
                    </GridCell>
                </GridInner>
                <CardActions>
                    <CardActionButtons>
                        <Button onClick={() => this.save()}>Save</Button>
                        <Button onClick={() => this.props.onCancel()}>Cancel</Button>
                        <EmailNotification id={this.state.event.eventId} type='event' />
                    </CardActionButtons>
                </CardActions>
            </Card>
        );
    }
}
