import { Card } from '@rmwc/card';
import { Checkbox } from '@rmwc/checkbox';
import { GridCell, GridInner } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Api from '../../api';
import IEvent from '../../interfaces/IEvent';
import { IStore } from '../../redux/store';
import EventView from './view';

interface IEventListProps extends IStore {}

interface IEventListState {
    events: IEvent[];
    search: string;
    viewPastEvents: boolean;
    isSuperAdmin: boolean;
}

class EventList extends Component<IEventListProps, IEventListState> {
    constructor(props: IEventListProps) {
        super(props);
        this.state = {
            events: [],
            search: '',
            viewPastEvents: false,
            isSuperAdmin: false
        };
    }

    async componentDidMount() {
        await this.updateEvents();
        await this.updateIsAdmin();
    }

    async componentDidUpdate(prevProps: IEventListProps) {
        if (prevProps.profile.userId === this.props.profile.userId) return;

        await this.updateIsAdmin();
    }

    async updateEvents() {
        try {
            let events = await Api.call(`/events`);
            this.setState({ events });
        } catch (e) {}
    }

    async updateIsAdmin() {
        if (!this.props.profile.userId) return;

        try {
            let isSuperAdmin = await Api.call(`/admins/${this.props.profile.userId}`);
            this.setState({ isSuperAdmin });
        } catch (e) {}
    }

    render() {
        return (
            <React.Fragment>
                <GridCell span={8}>
                    <Card className='margin-bottom'>
                        <div className='header'>
                            <Typography use='headline4'>Events</Typography>
                            {this.state.isSuperAdmin && (
                                <Link to={'/events/new'} className='flex align-center'>
                                    <Icon role='button' icon='add' style={{ marginLeft: 'auto' }} />
                                    <Typography use='headline6'>Add Event</Typography>
                                </Link>
                            )}
                        </div>
                        <div className='flex padded'>
                            <TextField
                                className='grow margin-right'
                                label={'Search events'}
                                trailingIcon='search'
                                outlined
                                value={this.state.search}
                                onChange={e => this.setState({ search: e.currentTarget.value })}
                            />
                            <Checkbox
                                label='View past events'
                                checked={this.state.viewPastEvents}
                                onChange={e =>
                                    this.setState({
                                        viewPastEvents: e.currentTarget.checked
                                    })
                                }
                            />
                        </div>
                    </Card>
                </GridCell>
                <GridCell span={12}>
                    <GridInner>
                        {this.state.events
                            .filter(e => e.name.toUpperCase().includes(this.state.search.toUpperCase()))
                            .filter(e => (this.state.viewPastEvents ? true : e.end > new Date()))
                            .map(event => (
                                <GridCell span={4} key={event.eventId}>
                                    <EventView event={event} />
                                </GridCell>
                            ))}
                    </GridInner>
                </GridCell>
            </React.Fragment>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(EventList);
