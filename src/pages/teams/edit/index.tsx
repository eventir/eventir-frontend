import { Card } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { Typography } from '@rmwc/typography';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import Api from '../../../api';
import TeamInfo from '../../../components/TeamInfo/TeamInfo';
import UserCard from '../../../components/UserCard/UserCard';
import ITeam from '../../../interfaces/ITeam';
import IUser from '../../../interfaces/IUser';

interface ITeamProps extends RouteComponentProps<{ id: string }> {}
interface ITeamState {
    team: ITeam;
}

export default class TeamEdit extends React.Component<ITeamProps, ITeamState> {
    constructor(props: ITeamProps) {
        super(props);
        this.state = {
            team: {
                teamId: -1,
                name: '',
                teamAdmin: {
                    userId: '',
                    firstName: '',
                    lastName: '',
                    profileIcon: '',
                    email: ''
                },
                members: []
            }
        };

        this.handleCancel = this.handleCancel.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        await this.updateTeam();
    }

    async componentDidUpdate(prevProps: ITeamProps) {
        if (prevProps.match.params.id === this.props.match.params.id) return;

        await this.updateTeam();
    }

    async updateTeam() {
        let teamId = Number.parseInt(this.props.match.params.id);
        if (!Number.isInteger(teamId)) return;

        try {
            let team = await Api.call(`/teams/${teamId}`);
            this.setState({ team });
        } catch (e) {}
    }

    async handleSave(team: ITeam) {
        this.setState({ team });

        let method = 'POST';
        let url = '/teams';
        if (this.state.team.teamId >= 0) {
            method = 'PUT';
            url += '/' + this.state.team.teamId;
        }

        if (team.teamAdmin != null) {
            await Api.call(`/teams/${this.state.team.teamId}/admins/${team.teamAdmin.userId}`, { method: 'POST' });
        }

        let res = await Api.call(url, { method, body: this.state.team });
        if (res.teamId) {
            this.props.history.push('/teams');
        }
    }

    async addMember(user: IUser) {
        await Api.call(`/teams/${this.state.team.teamId}/members/${user.userId}`, { method: 'POST' });
    }

    async removeMember(user: IUser) {
        await Api.call(`/teams/${this.state.team.teamId}/members/${user.userId}`, { method: 'DELETE' });
    }

    async handleDelete(team: ITeam) {
        this.setState({ team });

        if (this.state.team.members.length > 0) {
            alert('Cannot delete team with members!');
        } else {
            await Api.call(`/teams/${this.state.team.teamId}`, {
                method: 'DELETE'
            });
            alert('Deleted');
            this.props.history.push('./');
        }
    }
    handleCancel() {
        this.props.history.goBack();
    }

    render() {
        let teamMembers = this.state.team.members;

        return (
            <GridCell span={8}>
                <TeamInfo
                    handleSave={this.handleSave}
                    handleDelete={this.handleDelete}
                    handleCancel={this.handleCancel}
                    addMember={this.addMember}
                    onChange={team => this.setState({ team })}
                    team={this.state.team}
                    new={this.props.match.params.id == 'new'}
                />
                <br />
                <Card className='header margin-bottom'>
                    <Typography use='headline5'>Team Members</Typography>
                </Card>
                <GridInner id='user-grid'>
                    {teamMembers.map(user => (
                        <GridCell key={user.userId}>
                            <UserCard
                                user={user}
                                removeMember={(user: IUser) => {
                                    this.removeMember(user);
                                }}
                            />
                        </GridCell>
                    ))}
                </GridInner>
            </GridCell>
        );
    }
}
