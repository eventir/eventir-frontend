import { Card } from '@rmwc/card';
import { Checkbox } from '@rmwc/checkbox';
import { GridCell } from '@rmwc/grid';
import { Icon } from '@rmwc/icon';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

import Api from '../../api';
import TeamList from '../../components/teams/team-list';
import UserTeamList from '../../components/teams/user-team-list';
import ITeam from '../../interfaces/ITeam';

interface IMyState {
    teams: Array<ITeam>;
    myTeams: Array<ITeam>;
    search: string;
    isAdmin: boolean;
    isPreview: boolean;
}

export default class Teams extends Component<RouteComponentProps, IMyState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            teams: [],
            myTeams: [],
            search: '',
            isAdmin: false,
            isPreview: false
        };
    }

    async componentDidMount() {
        await Api.call('/users/current').then(async user => {
            await Api.call('/admins/' + user.userId).then(isAdmin => {
                this.setState({ isAdmin: isAdmin });

                if (this.state.isAdmin) {
                    Api.call('/teams/').then(teams => {
                        this.setState({ teams: teams });
                    });
                    Api.call('/users/' + user.userId + '/teams').then(teams => {
                        this.setState({ myTeams: teams });
                    });
                } else {
                    Api.call('/users/' + user.userId + '/teams').then(teams => {
                        this.setState({ myTeams: teams });
                    });
                }
            });
        });
    }

    render() {
        let viewMyTeamsCheckbox = null;
        if (this.state.isAdmin) {
            viewMyTeamsCheckbox = (
                <Checkbox
                    label='View My Teams'
                    checked={this.state.isPreview}
                    onChange={e =>
                        this.setState({
                            isPreview: e.currentTarget.checked
                        })
                    }
                />
            );
        }
        return (
            <GridCell span={8}>
                {(this.state.isAdmin && !this.state.isPreview && (
                    <Card>
                        <div className='header'>
                            <Typography use='headline4'>Teams</Typography>
                            <div className='flex flex-row align-center'>
                                <Link className='unstyled flex align-center' to='/team/new'>
                                    <Icon className='addIcon' role='button' icon='add' />
                                    <Typography use='subtitle1'>Add Team</Typography>
                                </Link>
                            </div>
                        </div>
                        <div className='flex align-center padded'>
                            <TextField
                                className='grow'
                                label={'Search Teams'}
                                trailingIcon={<Icon role='button' icon='search' />}
                                outlined
                                value={this.state.search}
                                onChange={e => this.setState({ search: e.currentTarget.value })}
                            />
                            {viewMyTeamsCheckbox}
                        </div>
                        <div className='padded'>
                            <TeamList teams={this.state.teams} search={this.state.search} />
                        </div>
                    </Card>
                )) || (
                    <Card className='constrained-card'>
                        <div className='header'>
                            <Typography use='headline4'>My Teams</Typography>
                        </div>
                        <div className='flex align-center padded'>
                            <TextField
                                className='grow'
                                label={'Search Teams'}
                                trailingIcon={<Icon role='button' icon='search' />}
                                outlined
                                value={this.state.search}
                                onChange={e => this.setState({ search: e.currentTarget.value })}
                            />
                            {viewMyTeamsCheckbox}
                        </div>
                        <div className='padded'>
                            <UserTeamList teams={this.state.myTeams} search={this.state.search} />
                        </div>
                    </Card>
                )}
            </GridCell>
        );
    }
}
