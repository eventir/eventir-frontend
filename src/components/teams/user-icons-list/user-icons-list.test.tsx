import React from 'react';
import ReactDOM from 'react-dom';

import UserIconsList from './index';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<UserIconsList users={[]} />, div);
    ReactDOM.unmountComponentAtNode(div);
});
