import './user-icons-list.scss';

import { Avatar } from '@rmwc/avatar';
import React, { Component } from 'react';

import IUser from '../../../interfaces/IUser';

interface IUserIconsListProps {
    users: Array<IUser>;
}

export default class UserIconsList extends Component<IUserIconsListProps, {}> {
    constructor(props: IUserIconsListProps) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.users.map(user => (
                    <Avatar
                        className='margin-right'
                        src={user.profileIcon}
                        title={'image of ' + user.lastName}
                        key={user.userId}
                    />
                ))}
            </div>
        );
    }
}
