import './user-team-list.scss';

import { Card, CardPrimaryAction } from '@rmwc/card';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import ITeam from '../../../interfaces/ITeam';
import UserIconsList from '../user-icons-list/index';

interface ITeamsListProps {
    teams: Array<ITeam>;
    search: string;
}

export default class UserTeamList extends Component<ITeamsListProps, {}> {
    constructor(props: ITeamsListProps) {
        super(props);
    }

    render() {
        return this.props.teams
            .filter(t => t.name.toUpperCase().includes(this.props.search.toUpperCase()))
            .map(team => (
                <Card outlined key={team.teamId}>
                    <Link to={'/teams/' + team.teamId} className='width100'>
                        <CardPrimaryAction className='flex flex-row align-center team-list'>
                            <Typography use='headline5' className='no-margin flex'>
                                {team.name}
                            </Typography>
                            <UserIconsList users={team.members} />
                        </CardPrimaryAction>
                    </Link>
                </Card>
            ));
    }
}
