import './team-list.scss';

import { Card, CardPrimaryAction } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import ITeam from '../../../interfaces/ITeam';
import IUser from '../../../interfaces/IUser';
import UserIconsList from '../user-icons-list/index';

interface ITeamsListProps {
    teams: Array<ITeam>;
    search: string;
}

export default class TeamList extends Component<ITeamsListProps, {}> {
    constructor(props: ITeamsListProps) {
        super(props);
    }

    render() {
        return this.props.teams
            .filter(t => t.name.toUpperCase().includes(this.props.search.toUpperCase()))
            .map(team => (
                <Card outlined key={team.teamId} className='margin-bottom'>
                    <Link to={'/teams/' + team.teamId}>
                        <CardPrimaryAction className='flex flex-row align-center padded'>
                            <Typography use='headline5' className='margin-right'>
                                {team.name}
                            </Typography>
                            <UserIconsList users={team.members} />
                            <Link
                                to={'/teams/' + team.teamId}
                                style={{ marginLeft: 'auto' }}
                                className='flex align-center'
                            >
                                <Typography use='body1' className='margin-right'>
                                    Edit
                                </Typography>
                                <Icon className='editIcon' role='button' icon='edit' />
                            </Link>
                        </CardPrimaryAction>
                    </Link>
                </Card>
            ));
    }
}
