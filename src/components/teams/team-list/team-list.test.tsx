import React from 'react';
import ReactDOM from 'react-dom';

import TeamList from './index';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<TeamList teams={[]} search='' />, div);
    ReactDOM.unmountComponentAtNode(div);
});
