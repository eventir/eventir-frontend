import '../../index.scss';
import './TeamInfo.scss';

import { Button } from '@rmwc/button';
import { Card, CardActionButton, CardActionButtons, CardActions } from '@rmwc/card';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React from 'react';
import { Link } from 'react-router-dom';

import ITeam from '../../interfaces/ITeam';
import IUser from '../../interfaces/IUser';
import UserSearch from '../user-search';

interface ITeamProps {
    team: ITeam;
    new: boolean;

    handleSave: (team: ITeam) => void;
    handleDelete: (team: ITeam) => void;
    handleCancel: () => void;
    addMember: (user: IUser) => void;

    onChange: (team: ITeam) => void;
}

interface ITeamState {
    memDialogOpen: boolean;
    newMember: IUser;
}

class TeamInfo extends React.Component<ITeamProps, ITeamState> {
    handleClickOpen = () => {
        this.setState({ memDialogOpen: true });
    };

    handleClose = () => {
        this.setState({ memDialogOpen: false });
    };

    render() {
        let teamAdmin = this.props.team.teamAdmin;
        return (
            <Card>
                <div className='padded'>
                    <TextField
                        label='Title'
                        outlined
                        value={this.props.team.name}
                        onChange={e => {
                            this.props.onChange({
                                ...this.props.team,
                                name: e.currentTarget.value
                            });
                        }}
                    />
                    <br />
                    <br />
                    <Typography use='headline5' className='no-margin'>
                        Team Admin
                    </Typography>
                    <br />
                    <Typography use='body1'>
                        {teamAdmin ? `${teamAdmin.firstName} ${teamAdmin.lastName}` : 'None'}
                    </Typography>
                    <br />
                    <UserSearch
                        onChange={(user: IUser) => {
                            this.props.onChange({
                                ...this.props.team,
                                teamAdmin: user
                            });
                        }}
                    />
                </div>

                <CardActions>
                    <CardActionButtons>
                        <CardActionButton onClick={() => this.props.handleSave(this.props.team)}>SAVE</CardActionButton>
                        {!this.props.new && (
                            <CardActionButton onClick={() => this.props.handleDelete(this.props.team)}>
                                DELETE
                            </CardActionButton>
                        )}
                        <CardActionButton onClick={() => this.props.handleCancel()}>CANCEL</CardActionButton>
                        <Link to={`/team/${this.props.team.teamId}/user/new`}>
                            <CardActionButton raised>
                                Add User
                            </CardActionButton>
                        </Link>
                    </CardActionButtons>
                </CardActions>
            </Card >
        );
    }
}

export default TeamInfo;
