import { Card, CardActionButton, CardActionButtons, CardActions, CardMedia, CardPrimaryAction } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Api from '../../api';
import ISession from '../../interfaces/ISession';
import { IStore } from '../../redux/store';
import RsvpCheckin from '../rsvp-checkin';

interface ISessionCardProps extends IStore {
    session: ISession;
}

interface ISessionCardState {
    isAdmin: boolean;
}

class SessionCard extends React.Component<ISessionCardProps, ISessionCardState> {
    constructor(props: ISessionCardProps) {
        super(props);

        this.state = {
            isAdmin: false
        };
    }

    async componentDidMount() {
        await this.updateIsAdmin();
    }

    async componentDidUpdate(prevProps: ISessionCardProps) {
        let sessionChanged = prevProps.session.sessionId !== this.props.session.sessionId;
        let userChanged = prevProps.profile.userId !== this.props.profile.userId;
        if (sessionChanged || userChanged) await this.updateIsAdmin();
    }

    async updateIsAdmin() {
        if (!this.props.profile.userId) return;

        try {
            let isAdmin = await Api.call(
                `/sessions/${this.props.session.sessionId}/admins/${this.props.profile.userId}`
            );
            this.setState({ isAdmin });
        } catch (e) { }
    }

    render() {
        return (
            <Card>
                <Link to={'/events/' + this.props.session.eventId + '/sessions/' + this.props.session.sessionId}>
                    <CardPrimaryAction>
                        {this.props.session.image && (
                            <CardMedia sixteenByNine style={{ backgroundImage: `url(${this.props.session.image})` }} />
                        )}
                        <div className='padded'>
                            <Typography use='headline4'>{this.props.session.name}</Typography>
                            <br />
                            <Typography use='headline5'>{this.props.session.description}</Typography>
                            <br />
                            <br />
                            <Typography use='body1' className='flex align-center no-margin'>
                                <Icon icon='event' className='margin-right' />
                                {this.props.session.start.toLocaleDateString()}
                            </Typography>
                            <Typography use='body1' className='flex align-center no-margin'>
                                <Icon icon='access_time' className='margin-right' />
                                {this.props.session.start.toLocaleTimeString()}–
                                {this.props.session.end.toLocaleTimeString()}
                            </Typography>
                        </div>
                    </CardPrimaryAction>
                </Link>

                {this.state.isAdmin && (
                    <CardActions>
                        <CardActionButtons>
                            <Link to={`/events/${this.props.session.eventId}/sessions/${this.props.session.sessionId}`}>
                                <CardActionButton unelevated>
                                    Edit
                                </CardActionButton>
                            </Link>
                            <RsvpCheckin session={this.props.session} />
                        </CardActionButtons>
                    </CardActions>
                )}
            </Card>
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(SessionCard);
