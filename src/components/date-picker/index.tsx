import './date-picker.scss';

import { Button } from '@rmwc/button';
import { Card, CardActionButtons, CardActions } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { IconButton } from '@rmwc/icon-button';
import { Select } from '@rmwc/select';
import React, { Component } from 'react';

interface IDatePickerProps {
    value: Date;
    onSave: (date: Date) => void;
    onCancel: () => void;
}
interface IDatePickerState {
    date: Date;
}

export default class DatePicker extends Component<IDatePickerProps, IDatePickerState> {
    constructor(props: IDatePickerProps) {
        super(props);
        this.state = {
            date: new Date(this.props.value)
        };
    }

    static getDerivedStateFromProps(next: IDatePickerProps, state: IDatePickerState) {
        if (next.value !== state.date) {
            return {
                ...state,
                date: next.value
            };
        }
        return null;
    }

    getCurrentDaysInMonth() {
        const month = this.state.date.getMonth() + 1;
        const year = this.state.date.getFullYear();

        return new Date(year, month, 0).getDate();
    }

    getWeekOfMonth(date: Date) {
        const firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
        return Math.ceil((date.getDate() + firstDay) / 7);
    }

    changeMonth(amount: number) {
        let date = this.state.date;
        date.setMonth(date.getMonth() + amount);
        this.setState({ date });
    }

    changeYear(e: React.FormEvent<HTMLSelectElement>) {
        let date = this.state.date;
        date.setFullYear(Number.parseInt(e.currentTarget.value));
        this.setState({ date });
    }

    render() {
        const numDays = this.getCurrentDaysInMonth();
        let currentDate = new Date(this.state.date);
        currentDate.setDate(0);

        const weeks = new Array<Array<Date>>();
        weeks.push(new Array<Date>());

        do {
            currentDate.setDate(currentDate.getDate() + 1);

            if (currentDate.getDate() === 0) {
                weeks.push(new Array<Date>());
            }

            let currentWeek = weeks[weeks.length - 1];
            currentWeek.push(new Date(currentDate));
        } while (currentDate.getDate() < numDays);

        const years = [];
        const currentYear = this.props.value.getFullYear();
        for (let i = currentYear - 2; i <= currentYear + 2; i++) {
            years.push(i);
        }

        return (
            <Card className='date-picker'>
                <div className='flex align-center justify-space-between padded-half'>
                    <IconButton onClick={() => this.changeMonth(-1)}>
                        <Icon icon='arrow_back' />
                    </IconButton>
                    <div>
                        <span className='margin-right'>
                            {this.state.date.toLocaleString(navigator.language, { month: 'long' })}
                        </span>
                        <Select
                            label='Year'
                            outlined
                            onChange={e => this.changeYear(e)}
                            value={this.state.date.getFullYear() + ''}
                        >
                            {years.map(year => (
                                <option value={year}>{year}</option>
                            ))}
                        </Select>
                    </div>
                    <IconButton onClick={() => this.changeMonth(1)}>
                        <Icon icon='arrow_forward' />
                    </IconButton>
                </div>
                <div style={{ display: 'grid' }}>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '1 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        S
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '2 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        M
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '3 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        T
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '4 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        W
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '5 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        Th
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '6 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        F
                    </span>
                    <span
                        className='week-label'
                        style={{
                            gridColumn: '7 / span 1',
                            gridRow: '1 / span 1'
                        }}
                    >
                        S
                    </span>
                    {weeks.map(week =>
                        week.map(day => (
                            <Button
                                key={day.getDate()}
                                style={{
                                    gridColumn: `${day.getDay() + 1} / span 1`,
                                    gridRow: `${this.getWeekOfMonth(day) + 1} / span 1`
                                }}
                                outlined={day.getTime() === this.state.date.getTime()}
                                onClick={() => this.setState({ date: day })}
                            >
                                {day.getDate()}
                            </Button>
                        ))
                    )}
                </div>
                <CardActions>
                    <CardActionButtons>
                        <Button raised onClick={() => this.props.onSave(this.state.date)}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.onCancel()}>Cancel</Button>
                    </CardActionButtons>
                </CardActions>
            </Card>
        );
    }
}
