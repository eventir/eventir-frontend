import './UserCard.scss';

import { Card, CardActionButton, CardActionButtons, CardActions, CardMedia, CardPrimaryAction } from '@rmwc/card';
import { Typography } from '@rmwc/typography';
import React from 'react';
import { Link } from 'react-router-dom';

import IUser from '../../interfaces/IUser';

interface IUserCardProps {
    user: IUser;
    removeMember: (user: IUser) => void;
}

export default class UserCard extends React.Component<IUserCardProps, {}> {
    render() {
        return (
            <Card className='user-card'>
                <Link to={`/users/${this.props.user.userId}`}>
                    <CardPrimaryAction>
                        <CardMedia style={{ backgroundImage: `url(${this.props.user.profileIcon})` }} square />
                        <Typography use='headline5'>
                            {this.props.user.firstName + ' ' + this.props.user.lastName}
                        </Typography>
                    </CardPrimaryAction>
                </Link>
                <CardActions>
                    <CardActionButtons>
                        <Link to={`/users/${this.props.user.userId}`}>
                            <CardActionButton unelevated>
                                Edit
                            </CardActionButton>
                        </Link>
                    </CardActionButtons>
                </CardActions>
            </Card>
        );
    }
}
