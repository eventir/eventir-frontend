import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Store from '../../redux/store';
import RsvpCheckin from './index';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={Store}>
            <RsvpCheckin
                session={{
                    sessionId: 0,
                    eventId: 0,
                    name: '',
                    start: new Date(),
                    end: new Date()
                }}
            />
        </Provider>,
        div
    );
    ReactDOM.unmountComponentAtNode(div);
});
