import { Button } from '@rmwc/button';
import { Icon } from '@rmwc/icon';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { AnyAction, Dispatch } from 'redux';

import Api from '../../api';
import ICheckin from '../../interfaces/ICheckin';
import IEvent from '../../interfaces/IEvent';
import ISession from '../../interfaces/ISession';
import { addCheckins, removeCheckins } from '../../redux/actions/checkins';
import { addRSVPs, removeRSVPs } from '../../redux/actions/rsvps';
import { IStore } from '../../redux/store';

interface IRsvpCheckinProps extends IStore {
    session: ISession;
    addRSVPs: (...sessions: number[]) => void;
    removeRSVPs: (...sessions: number[]) => void;
    addCheckins: (...sessions: number[]) => void;
    removeCheckins: (...sessions: number[]) => void;
}

interface IRsvpCheckinState {
    event: IEvent;
}

class RsvpCheckin extends Component<IRsvpCheckinProps, IRsvpCheckinState> {
    constructor(props: IRsvpCheckinProps) {
        super(props);

        this.state = {
            event: {
                eventId: -1,
                name: '',
                description: '',
                start: new Date(),
                end: new Date(),
                image: '',
                checkinWindow: 0,
                sessions: [],
                eventAdmins: []
            }
        };
    }

    async componentDidUpdate(prevProps: IRsvpCheckinProps) {
        if (prevProps.session.eventId === this.props.session.eventId) return;

        try {
            let event = await Api.call(`/events/${this.props.session.eventId}`);
            this.setState({ event });
        } catch (e) {}
    }

    isRsvpd() {
        return this.props.userRsvps.includes(this.props.session.sessionId);
    }

    isCheckedIn() {
        return this.props.userCheckins.includes(this.props.session.sessionId);
    }

    canCheckIn() {
        const now = new Date();
        const allowedStartTime = new Date(this.props.session.start);
        allowedStartTime.setMinutes(allowedStartTime.getMinutes() - this.state.event.checkinWindow);
        const allowedEndTime = new Date(this.props.session.start);
        allowedEndTime.setMinutes(allowedEndTime.getMinutes() + this.state.event.checkinWindow);

        return allowedStartTime <= now && now <= allowedEndTime;
    }

    async rsvp() {
        let rsvp = await Api.call('/rsvps', {
            method: this.isRsvpd() ? 'DELETE' : 'POST',
            body: {
                sessionId: this.props.session.sessionId,
                userId: this.props.profile.userId
            }
        });

        if (rsvp && Number.isInteger(rsvp.rsvpId)) this.props.addRSVPs(this.props.session.sessionId);
        else this.props.removeRSVPs(this.props.session.sessionId);

        // TODO: show warning on fail
    }

    async checkin() {
        if (this.isCheckedIn()) return;

        let checkin = (await Api.call('/checkins', {
            method: 'POST',
            body: {
                sessionId: this.props.session.sessionId,
                userId: this.props.profile.userId,
                inTime: new Date() //TODO: remove and user server-side
            }
        })) as ICheckin;

        if (Number.isInteger(checkin.checkinId)) {
            this.props.addCheckins(this.props.session.sessionId);
        }
        // TODO: show warning on fail
    }

    render() {
        return (
            <Fragment>
                {this.canCheckIn() || this.isCheckedIn() ? (
                    <Button
                        disabled={this.isCheckedIn()}
                        onClick={() => {
                            this.checkin();
                        }}
                        icon={<Icon icon='check' />}
                    >
                        Check in
                    </Button>
                ) : (
                    <Button
                        onClick={() => {
                            this.rsvp();
                        }}
                        icon={<Icon icon={this.isRsvpd() ? 'bookmark' : 'bookmark_border'} />}
                    >
                        {this.isRsvpd() ? 'Cancel ' : ''}RSVP
                    </Button>
                )}
            </Fragment>
        );
    }
}

export default connect(
    (store: IStore) => ({
        ...store
    }),
    (dispatch: Dispatch<AnyAction>) => ({
        addRSVPs: (...sessionIds: number[]) => dispatch(addRSVPs(...sessionIds)),
        removeRSVPs: (...sessionIds: number[]) => dispatch(removeRSVPs(...sessionIds)),
        addCheckins: (...sessionIds: number[]) => dispatch(addCheckins(...sessionIds)),
        removeCheckins: (...sessionIds: number[]) => dispatch(removeCheckins(...sessionIds))
    })
)(RsvpCheckin);
