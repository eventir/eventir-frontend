import { Card, CardPrimaryAction } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';

import IEvent from '../../interfaces/IEvent';

interface IEventListProps {
    events: Array<IEvent>;
    search: string;
}

export default class SmallEventList extends Component<IEventListProps, {}> {
    constructor(props: IEventListProps) {
        super(props);
    }

    render() {
        return this.props.events
            .filter(e => e.name.toUpperCase().includes(this.props.search.toUpperCase()))
            .map(event => (
                <Card outlined className='margin-bottom' key={event.eventId}>
                    <CardPrimaryAction className='padded'>
                        <Typography use='subtitle1' className='no-margin'>
                            {event.name}
                        </Typography>
                        <Typography use='subtitle2' className='no-margin'>
                            {event.description}
                        </Typography>
                        <Typography use='caption' className='no-margin flex align-center'>
                            <Icon icon='date_range' className='margin-right' />
                            {event.start.toLocaleDateString() +
                                ' ' +
                                event.start.toLocaleTimeString('en-US', {
                                    hour: '2-digit',
                                    minute: '2-digit'
                                })}
                        </Typography>
                    </CardPrimaryAction>
                </Card>
            ));
    }
}
