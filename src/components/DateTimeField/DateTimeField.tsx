import { Grid, GridCell, GridInner } from '@rmwc/grid';
import { TextField } from '@rmwc/textfield';
import React from 'react';

interface IDateTimeFieldProps {
    labelPrefix: string | undefined;
    date: Date;
    onChange: (date: Date) => any;
}

interface IDateTimeFieldState {
    dateText: string;
    timeText: string;
}

export default class DateTimeField extends React.Component<IDateTimeFieldProps, IDateTimeFieldState> {
    constructor(props: IDateTimeFieldProps) {
        super(props);
        this.state = {
            dateText: this.formatDate(props.date),
            timeText: this.formatTime(props.date)
        };
    }

    componentDidUpdate(prevProps: IDateTimeFieldProps, prevState: IDateTimeFieldState) {
        if (this.props.date.getTime() !== prevProps.date.getTime()) {
            this.setState({
                dateText: this.formatDate(this.props.date),
                timeText: this.formatTime(this.props.date)
            });
        }

        let dateChanged = prevState.dateText !== this.state.dateText;
        let timeChanged = prevState.timeText !== this.state.timeText;
        if (dateChanged || timeChanged) {
            let newDate = new Date(this.state.dateText);
            let hourMinutes = this.state.timeText.split(':');
            newDate.setHours(Number.parseInt(hourMinutes[0]));
            newDate.setMinutes(Number.parseInt(hourMinutes[1]));
            if (
                newDate &&
                newDate.getTime() !== this.props.date.getTime() &&
                newDate.getTime() !== prevProps.date.getTime()
            ) {
                this.props.onChange(newDate);
            }
        }
    }

    formatDate(date: Date) {
        return `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
    }

    formatTime(date: Date) {
        return `${('0' + date.getHours()).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}`;
    }

    render() {
        return (
            <GridInner>
                <GridCell span={8}>
                    <TextField
                        label={`${this.props.labelPrefix} date`}
                        outlined
                        required
                        type='date'
                        value={this.state.dateText}
                        onChange={e => {
                            this.setState({
                                dateText: e.currentTarget.value
                            });
                        }}
                    />
                </GridCell>
                <GridCell span={4} tablet={8}>
                    <TextField
                        label={`${this.props.labelPrefix} time`}
                        outlined
                        required
                        type='time'
                        value={this.state.timeText}
                        onChange={e => {
                            this.setState({
                                timeText: e.currentTarget.value
                            });
                        }}
                    />
                </GridCell>
            </GridInner>
        );
    }
}
