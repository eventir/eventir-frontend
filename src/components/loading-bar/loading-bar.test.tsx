import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Store from '../../redux/store';
import LoadingBar from './index';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={Store}>
            <LoadingBar />
        </Provider>,
        div
    );
    ReactDOM.unmountComponentAtNode(div);
});
