import { LinearProgress } from '@rmwc/linear-progress';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { IStore } from '../../redux/store';

export class LoadingBar extends Component<IStore, {}> {
    constructor(props: IStore) {
        super(props);
    }

    render() {
        return (
            <LinearProgress
                closed={this.props.requests.total === 0}
                progress={1 - this.props.requests.left / this.props.requests.total}
            />
        );
    }
}

export default connect((store: IStore) => ({
    ...store
}))(LoadingBar);
