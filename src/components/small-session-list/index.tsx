import { Card, CardPrimaryAction } from '@rmwc/card';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';

import ISession from '../../interfaces/ISession';

interface ISessionListProps {
    sessions: ISession[];
    search: string;
}

export default class EventList extends Component<ISessionListProps, {}> {
    constructor(props: ISessionListProps) {
        super(props);
    }

    render() {
        return this.props.sessions
            .filter(s => s.name.toUpperCase().includes(this.props.search.toUpperCase()))
            .map(session => (
                <Card className='session-card' key={session.sessionId}>
                    <CardPrimaryAction className='padded-half'>
                        <Typography use='subtitle1' className='no-margin'>
                            {session.name}
                        </Typography>
                        <Typography use='subtitle2' className='no-margin'>
                            {session.description}
                        </Typography>
                        <Typography use='caption' className='flex align-center'>
                            <Icon icon='date_range' className='margin-right' />
                            {session.start.toLocaleDateString() +
                                ' ' +
                                session.start.toLocaleTimeString('en-US', {
                                    hour: '2-digit',
                                    minute: '2-digit'
                                })}
                        </Typography>
                        <Typography use='caption' className='flex align-center'>
                            <Icon icon='location_on' className='margin-right' />
                            {session.location}
                        </Typography>
                    </CardPrimaryAction>
                </Card>
            ));
    }
}
