import { Card, CardMedia, CardPrimaryAction } from '@rmwc/card';
import { GridCell, GridInner } from '@rmwc/grid';
import { TextField } from '@rmwc/textfield';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';

import Api from '../../api';
import IUser from '../../interfaces/IUser';

interface IUserSearchProps {
    onChange: (user: IUser) => void;
}

interface IUserSearchState {
    search: string;
    users: IUser[];
}

export default class UserSearch extends Component<IUserSearchProps, IUserSearchState> {
    constructor(props: IUserSearchProps) {
        super(props);

        this.state = {
            search: '',
            users: []
        };
    }

    async searchUsers() {
        if (this.state.search.trim().length === 0) return;

        let users = await Api.call(`/users/search/${this.state.search}`);
        if (users.length >= 0) {
            this.setState({ users });
        }
    }

    render() {
        return (
            <div>
                <TextField
                    label='Users'
                    icon='search'
                    value={this.state.search}
                    onChange={e => {
                        this.setState({ search: e.currentTarget.value });
                        this.searchUsers();
                    }}
                />
                <GridInner>
                    {this.state.users.map(u => (
                        <GridCell span={4} key={u.userId}>
                            <Card>
                                <CardPrimaryAction onClick={() => this.props.onChange(u)}>
                                    <CardMedia square style={{ backgroundImage: `url${u.profileIcon})` }} />
                                    <Typography use='body2'>
                                        {u.firstName} {u.lastName}
                                    </Typography>
                                </CardPrimaryAction>
                            </Card>
                        </GridCell>
                    ))}
                </GridInner>
            </div>
        );
    }
}
