import './user-list.scss';

import { Card, CardPrimaryAction } from '@rmwc/card';
import { Typography } from '@rmwc/typography';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import IUser from '../../../interfaces/IUser';

interface IUserListProps {
    users: Array<IUser>;
    search: string;
}

export default class UserList extends Component<IUserListProps, {}> {
    constructor(props: IUserListProps) {
        super(props);
    }

    render() {
        return this.props.users
            .filter(u => (u.firstName + u.lastName).toUpperCase().includes(this.props.search.toUpperCase()))
            .map(user => (
                <Card outlined key={user.userId}>
                    <Link to={'/users/' + user.userId} className='width100'>
                        <CardPrimaryAction className='flex flex-row align-center team-list'>
                            <div className='usercontent'>
                                <img className='usericon' src={user.profileIcon} />
                                <Typography use='headline5' className='no-margin flex'>
                                    {user.firstName + ' ' + user.lastName}
                                </Typography>
                            </div>
                        </CardPrimaryAction>
                    </Link>
                </Card>
            ));
    }
}
