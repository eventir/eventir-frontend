import 'react-image-crop/lib/ReactCrop.scss';

import './image-upload.scss';

import { Dialog, DialogActions, DialogButton, DialogContent } from '@rmwc/dialog';
import { Icon } from '@rmwc/icon';
import { Typography } from '@rmwc/typography';
import React, { Component, Fragment } from 'react';
import Dropzone from 'react-dropzone';
import ReactCrop, { Crop, PixelCrop } from 'react-image-crop';
import { connect } from 'react-redux';
import { AnyAction, Dispatch } from 'redux';

import Api from '../../api';
import IUser from '../../interfaces/IUser';
import { setProfile } from '../../redux/actions/login';
import { IStore } from '../../redux/store';

interface IImageUploadProps extends IStore {
    src: string;
    square?: boolean;
    minWidth?: number;
    minHeight?: number;
    onChange: (url: string) => void;
}

interface IImageUploadState {
    crop: Crop;
    pixelCrop?: PixelCrop;
    uploadUrl?: any;
    filename?: string;
}

class ImageUpload extends Component<IImageUploadProps, IImageUploadState> {
    readonly reader = new FileReader();
    readonly canvas = document.createElement('canvas');

    constructor(props: IImageUploadProps) {
        super(props);

        this.state = {
            crop: {
                x: 0,
                y: 0
            }
        };

        this.reader.onload = e => {
            this.setState({ uploadUrl: this.reader.result });
        };
        // TODO: handle upload error
    }

    static getDerivedStateFromProps(next: IImageUploadProps, state: IImageUploadState) {
        if (next.square)
            return {
                ...state,
                crop: {
                    ...state.crop,
                    width: undefined,
                    height: undefined
                }
            };
        return null;
    }

    handleDrop(files: File[]) {
        const file = files[0];
        this.setState({ filename: file.name });
        this.setState({
            crop: {
                x: 0,
                y: 0
            }
        });
        this.reader.readAsDataURL(file);
    }

    handleCrop(action?: string) {
        if (action != 'complete') return;

        let img = new Image();
        img.src = this.state.uploadUrl;

        let crop = this.state.pixelCrop || {
            x: 0,
            y: 0,
            width: img.width,
            height: img.height
        };

        this.canvas.width = crop.width;
        this.canvas.height = crop.height;

        const ctx = this.canvas.getContext('2d');
        if (ctx == null) return;

        ctx.drawImage(img, crop.x, crop.y, crop.width, crop.height, 0, 0, crop.width, crop.height);

        this.canvas.toBlob(async b => {
            if (b != null) {
                let data = new FormData();
                data.append('image', b);
                let response = await fetch(Api.BaseUrl + '/image', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Access-Control-Allow-Headers': 'Content-Type'
                    },
                    mode: 'cors',
                    credentials: 'include',
                    body: data
                });
                try {
                    let url = await response.text();
                    this.props.onChange(`${JSON.parse(url)}`);
                } catch (e) {
                    // TODO: Handle error case.
                }
            }
        });

        this.setState({ uploadUrl: undefined });
    }

    render() {
        return (
            <Fragment>
                <Dialog onClose={e => this.handleCrop(e.detail.action)} open={this.state.uploadUrl}>
                    <DialogContent>
                        {this.state.uploadUrl && (
                            <ReactCrop
                                src={this.state.uploadUrl}
                                onChange={(crop, pixelCrop) => {
                                    this.setState({ crop, pixelCrop });
                                }}
                                crop={this.state.crop}
                                minHeight={this.props.minHeight}
                                minWidth={this.props.minWidth}
                            />
                        )}
                    </DialogContent>
                    <DialogActions>
                        <DialogButton action='cancel'>Cancel</DialogButton>
                        <DialogButton action='complete' isDefaultAction>
                            Complete
                        </DialogButton>
                    </DialogActions>
                </Dialog>
                <div className='image-upload'>
                    <Dropzone onDrop={files => this.handleDrop(files)} accept='image/*'>
                        {({ getRootProps, getInputProps }) => (
                            <div>
                                <img src={this.props.src || 'https://via.placeholder.com/256'} className='responsive' />
                                <div {...getRootProps()} className='hov'>
                                    <input {...getInputProps()} />
                                    <Icon icon='cloud_upload' />
                                    <Typography use='caption'>Drag an image here or click to upload</Typography>
                                </div>
                            </div>
                        )}
                    </Dropzone>
                </div>
            </Fragment>
        );
    }
}

export default connect(
    (store: IStore) => ({
        ...store
    }),
    (dispatch: Dispatch<AnyAction>) => ({
        setProfile: (profile: IUser) => dispatch(setProfile(profile))
    })
)(ImageUpload);
