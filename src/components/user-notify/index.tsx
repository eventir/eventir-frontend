import { Button } from '@rmwc/button';
import { Dialog, DialogActions, DialogButton, DialogContent, DialogTitle } from '@rmwc/dialog';
import { Grid, GridCell, GridInner } from '@rmwc/grid';
import { TextField } from '@rmwc/textfield';
import React, { Component } from 'react';

import Api from '../../api';

interface IEmailNotificationProps {
    id: number;
    type: 'event' | 'session' | 'all';
}

interface IEmailNotificationState {
    isOpen: boolean;
    title: string;
    body: string;
}

export default class EmailNotification extends React.Component<IEmailNotificationProps, IEmailNotificationState> {
    constructor(props: IEmailNotificationProps) {
        super(props);

        this.state = {
            isOpen: false,
            title: '',
            body: ''
        };
    }

    async routeNotification() {
        let url = '/notify/' + this.props.type;
        if (this.state.title && this.state.body) {
            if (this.props.id) {
                url += '/' + this.props.id;
            }

            Api.call(url, {
                method: 'POST',
                body: { title: this.state.title, body: this.state.body }
            });
        }
    }

    handleClickOpen = () => {
        this.setState({ isOpen: true });
    };

    handleClose(e: CustomEvent<{ action?: string }>) {
        switch (e.detail.action) {
            case 'send':
                this.routeNotification();
        }
        this.setState({ isOpen: false });
    }

    render() {
        return (
            <React.Fragment>
                <Button onClick={this.handleClickOpen}>Notify All</Button>
                <Dialog
                    open={this.state.isOpen}
                    onClose={e => {
                        this.handleClose(e);
                    }}
                >
                    <DialogTitle>New Notification</DialogTitle>
                    <DialogContent>
                        <GridInner>
                            <GridCell span={12}>
                                <TextField
                                    label='Title'
                                    outlined
                                    className='width100'
                                    value={this.state.title}
                                    onChange={e =>
                                        this.setState({
                                            title: e.currentTarget.value
                                        })
                                    }
                                />
                            </GridCell>
                            <GridCell span={12}>
                                <TextField
                                    label='Body'
                                    textarea
                                    outlined
                                    value={this.state.body}
                                    onChange={e =>
                                        this.setState({
                                            body: e.currentTarget.value
                                        })
                                    }
                                />
                            </GridCell>
                        </GridInner>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton action='cancel' color='primary'>
                            Cancel
                        </DialogButton>
                        <DialogButton action='send' color='primary'>
                            Send
                        </DialogButton>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}
