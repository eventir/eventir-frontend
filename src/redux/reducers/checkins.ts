import { AnyAction } from 'redux';

import ISession from '../../interfaces/ISession';
import { CheckinActionTypes } from '../actions/checkins';

export function userCheckins(state: number[] = [], action: AnyAction) {
    switch (action.type) {
        case CheckinActionTypes.ADD_CHECKINS:
            return state.concat(action.sessionIds);
        case CheckinActionTypes.REMOVE_CHECKINS:
            return state.filter(sessionId => !action.sessionIds.includes(sessionId));
    }
    return state;
}
