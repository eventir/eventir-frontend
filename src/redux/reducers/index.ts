import { combineReducers } from 'redux';

import * as ApiReducers from './api';
import * as CheckinReducers from './checkins';
import * as LoginReducers from './login';
import * as RsvpReducers from './rsvps';

const Reducers = combineReducers({
    ...LoginReducers,
    ...ApiReducers,
    ...RsvpReducers,
    ...CheckinReducers
});
export default Reducers;
