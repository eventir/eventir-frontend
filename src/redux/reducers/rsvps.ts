import { AnyAction } from 'redux';

import { RsvpActionTypes } from '../actions/rsvps';

export function userRsvps(state: number[] = [], action: AnyAction) {
    switch (action.type) {
        case RsvpActionTypes.ADD_RSVPS:
            return state.concat(action.sessionIds);
        case RsvpActionTypes.REMOVE_RSVPS:
            return state.filter(sessionId => !action.sessionIds.includes(sessionId));
    }
    return state;
}
