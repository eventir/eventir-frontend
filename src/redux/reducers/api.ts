import { AnyAction } from 'redux';

import { ApiActionTypes } from '../actions/api';

export function requests(state = { total: 0, left: 0 }, action: AnyAction) {
    switch (action.type) {
        case ApiActionTypes.ADD_REQUEST:
            return {
                total: state.total + 1,
                left: state.left + 1
            };

        case ApiActionTypes.RESOLVE_REQUEST:
            if (state.left == 1)
                return {
                    total: 0,
                    left: 0
                };
            else if (state.left > 0)
                return {
                    total: state.total,
                    left: state.left - 1
                };
    }
    return state;
}
