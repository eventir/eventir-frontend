import { AnyAction } from 'redux';

import IUser from '../../interfaces/IUser';
import { LoginActionTypes } from '../actions/login';

export function loggedIn(state = false, action: AnyAction) {
    switch (action.type) {
        case LoginActionTypes.SET_LOGGED_IN:
            return action.loggedIn;
    }
    return state;
}

export function profile(
    state: IUser = { userId: '', firstName: '', lastName: '', profileIcon: '', email: '' },
    action: AnyAction
) {
    if (action.type == LoginActionTypes.SET_PROFILE) {
        return {
            ...action.profile
        };
    }
    return state;
}

export function token(state = '', action: AnyAction) {
    switch (action.type) {
        case LoginActionTypes.SET_TOKEN:
            return action.token;
    }
    return state;
}
