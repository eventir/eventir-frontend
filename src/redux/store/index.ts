import { AnyAction, applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import thunkMiddleware from 'redux-thunk';

import IUser from '../../interfaces/IUser';
import Reducers from '../reducers';

export interface IStore {
    loggedIn: boolean;
    token: string;
    profile: IUser;
    requests: {
        total: number;
        left: number;
    };
    userRsvps: number[];
    userCheckins: number[];
}

const Store = createStore<IStore, AnyAction, {}, {}>(Reducers, composeWithDevTools(applyMiddleware(thunkMiddleware)));

if (module.hot) {
    module.hot.accept('../reducers', () => {
        const nextReducer = require('../reducers');
        Store.replaceReducer(nextReducer);
    });
}

export default Store;
