import { AnyAction } from 'redux';

export enum CheckinActionTypes {
    ADD_CHECKINS = 'ADD_CHECKINS',
    REMOVE_CHECKINS = 'REMOVE_CHECKINS'
}

export const addCheckins = (...sessionIds: number[]): AnyAction => ({
    type: CheckinActionTypes.ADD_CHECKINS,
    sessionIds
});

export const removeCheckins = (...sessionIds: number[]): AnyAction => ({
    type: CheckinActionTypes.REMOVE_CHECKINS,
    sessionIds
});
