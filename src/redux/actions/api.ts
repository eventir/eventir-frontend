import { AnyAction } from 'redux';

export enum ApiActionTypes {
    ADD_REQUEST = 'ADD_REQUEST',
    RESOLVE_REQUEST = 'RESOLVE_REQUEST'
}

export const addRequest = (): AnyAction => ({
    type: ApiActionTypes.ADD_REQUEST
});

export const resolveRequest = (): AnyAction => ({
    type: ApiActionTypes.RESOLVE_REQUEST
});
