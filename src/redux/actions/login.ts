import { AnyAction } from 'redux';

import IUser from '../../interfaces/IUser';

export enum LoginActionTypes {
    SET_LOGGED_IN = 'SET_LOGGED_IN',
    SET_PROFILE = 'SET_PROFILE',
    SET_TOKEN = 'SET_TOKEN'
}

export const setLoggedIn = (loggedIn: boolean): AnyAction => ({
    type: LoginActionTypes.SET_LOGGED_IN,
    loggedIn
});

export const setProfile = (profile: IUser): AnyAction => ({
    type: LoginActionTypes.SET_PROFILE,
    profile
});

export const setToken = (token: string): AnyAction => ({
    type: LoginActionTypes.SET_TOKEN,
    token
});
