import { AnyAction } from 'redux';

export enum RsvpActionTypes {
    ADD_RSVPS = 'ADD_RSVPS',
    REMOVE_RSVPS = 'REMOVE_RSVPS'
}

export const addRSVPs = (...sessionIds: number[]): AnyAction => ({
    type: RsvpActionTypes.ADD_RSVPS,
    sessionIds
});

export const removeRSVPs = (...sessionIds: number[]): AnyAction => ({
    type: RsvpActionTypes.REMOVE_RSVPS,
    sessionIds
});
