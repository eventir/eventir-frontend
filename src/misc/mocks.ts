export function mockMutOb() {
    //eslint-disable-next-line
    (global as any).MutationObserver = class {
        constructor(callback: any) {} //eslint-disable-line
        disconnect() {}
        observe(element: any, initObject: any) {}
        takeRecords() {
            return [];
        }
    };
}
